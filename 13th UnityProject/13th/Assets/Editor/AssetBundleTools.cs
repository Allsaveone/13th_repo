﻿#if UNITY_EDITOR //Editor scripts will cause errors in the build process if not properly wrapped
using UnityEngine;
using UnityEditor;
using System;

public class AssetBundleTools {

    [MenuItem("Tools/AssetBundles/Build Asset Bundles")]//The notation for this function on the Unity menu drop down
    static void BuildAllAssetBundles()
    {
        //Bundles must be tailored to specific Build Targets
        BuildPipeline.BuildAssetBundles("Assets/ProjectAssets/Bundles/"+ EditorUserBuildSettings.activeBuildTarget.ToString(),//Build to the Bundle folder for this build target
            BuildAssetBundleOptions.None,
            EditorUserBuildSettings.activeBuildTarget); //build a bundle for this build target.
    }
}
#endif
