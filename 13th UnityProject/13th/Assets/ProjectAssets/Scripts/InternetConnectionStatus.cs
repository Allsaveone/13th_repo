﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class InternetConnectionStatus : MonoBehaviour {

    public bool internetConnection {get; private set;}
    public float maxTime = 1.0F;
    public float timeTaken = 0.0F;

    void OnEnable()
    {
        internetConnection = false;

        if (this.isActiveAndEnabled)
            StartCoroutine(ConnectionTest());
    }

    IEnumerator ConnectionTest()
    {
        while (true)
        {
            yield return new WaitForSeconds(2f);

            Ping connectionTest = new Ping("8.8.8.8");//Google
            Debug.Log("ICS | Testing Connection Status......." + (internetConnection == true ? "Connnected" : "Disconnnected"));
           
            timeTaken = 0.0F;

            while (!connectionTest.isDone)
            {
                timeTaken += Time.deltaTime;

                if (timeTaken > maxTime)
                {
                    internetConnection = false;
                    break;
                }
                
                yield return null;
            }

            if (timeTaken <= maxTime)
            {
                if (!internetConnection)
                {
                    Debug.Log("ICS-Connected");
                }

                internetConnection = true;
            }
            yield return null;
        }
    }

    void OnDisable()
    {
        StopCoroutine(ConnectionTest());
    }

}
