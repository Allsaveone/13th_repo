﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsManager : MonoBehaviour {

    [Header("Graphic Settings")]
    public Toggle fullScreenToggle;
    public Dropdown graphicsDropDown;
    public Dropdown resolutionDropDown;
    public Dropdown textureQualityDropDown;
    public Dropdown antiAliasingDropDown;
    public Dropdown vSyncDropDown;

    [Header("Audio Settings")]
    public Slider musicVolumeSlider;
    public Slider sfxVolumeSlider;

    [Header("Setting Buttons")]
    public Resolution[] resolutions;
    public string[] qualitySettings;
    public GameSettings gameSettings;

    void OnEnable ()
    {
        gameSettings = new GameSettings();

        resolutions = Screen.resolutions;
       
        List<Dropdown.OptionData> resolutionOptions = new List<Dropdown.OptionData>();

        for (int i = 0; i < resolutions.Length; i++)
        {
            Dropdown.OptionData option = new Dropdown.OptionData();
            option.text = resolutions[i].ToString();
            resolutionOptions.Add(option);
        }

        Debug.LogWarning(resolutions.Length);
        resolutionDropDown.ClearOptions();
        resolutionDropDown.AddOptions(resolutionOptions);

        qualitySettings = QualitySettings.names;

        SetListeners();
    }

    void SetListeners()
    {
        fullScreenToggle.onValueChanged.AddListener(delegate { OnFullScreenToggle(); });
        graphicsDropDown.onValueChanged.AddListener(delegate { OnGraphicsChanged(); });
        resolutionDropDown.onValueChanged.AddListener(delegate { OnResolutionChanged(); });
        textureQualityDropDown.onValueChanged.AddListener(delegate { OnTextureQualityChanged(); });
        antiAliasingDropDown.onValueChanged.AddListener(delegate { OnAntiAliasingChanged(); });
        vSyncDropDown.onValueChanged.AddListener(delegate { OnVsyncChanged(); });

        musicVolumeSlider.onValueChanged.AddListener(delegate { OnMusicVolumeChanged(); });
        sfxVolumeSlider.onValueChanged.AddListener(delegate { OnSfxVolumeChanged(); });

    }

    public void OnFullScreenToggle ()
    {
        gameSettings.fullScreen = fullScreenToggle.isOn;
        Screen.fullScreen = gameSettings.fullScreen;
	}

    public void OnGraphicsChanged()
    {
        gameSettings.graphicsIndex = graphicsDropDown.value;
        QualitySettings.SetQualityLevel(gameSettings.graphicsIndex);
        Debug.LogWarning("OnGraphicsChanged: " + QualitySettings.names[QualitySettings.GetQualityLevel()]);
    }

    public void OnResolutionChanged()
    {
        gameSettings.resolutionIndex = resolutionDropDown.value;
        //Screen.SetResolution();
    }

    public void OnTextureQualityChanged()
    {
        gameSettings.textureQualityIndex = textureQualityDropDown.value;
        QualitySettings.masterTextureLimit = gameSettings.textureQualityIndex;
    }

    public void OnAntiAliasingChanged()
    {
        gameSettings.antiAliasingIndex = antiAliasingDropDown.value; //DD
        QualitySettings.antiAliasing = (int)Mathf.Pow(2, gameSettings.antiAliasingIndex);
    }

    public void OnVsyncChanged()
    {
        gameSettings.vSyncIndex = antiAliasingDropDown.value;
        QualitySettings.vSyncCount = gameSettings.vSyncIndex;
    }

    public void OnMusicVolumeChanged()
    {
        gameSettings.musicVolume = musicVolumeSlider.value;
    }

    public void OnSfxVolumeChanged()
    {
        gameSettings.sfxVolume = sfxVolumeSlider.value;
    }
}

public class GameSettings
{

    //Basic Visual Options
    public bool fullScreen;
    public int graphicsIndex;
    public int resolutionIndex;
    public int textureQualityIndex;
    public int antiAliasingIndex;
    public int vSyncIndex;
    //PostFX controls

    //Basic Audio Settings
    //public float masterVolume;
    public float musicVolume;
    public float sfxVolume;
}
