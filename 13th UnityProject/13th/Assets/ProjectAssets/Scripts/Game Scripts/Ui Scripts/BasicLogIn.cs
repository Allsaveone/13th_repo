﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BasicLogIn : MonoBehaviour {

    public GameObject LogIn_UiHolder;
    public InputField userName_Input;
    public InputField passWord_Input;
    public Button logIn_Button;
    public Button register_Button;
    public Button quit_Button;

    public GameObject Game_UiHolder;
    public InputField score_input;

    private SparkManager sparkManager;

    void Start ()
    {
        sparkManager = GameObject.Find("SparkManager").GetComponent<SparkManager>();

        if (sparkManager == null)
        {
            Debug.LogError("SparkManager not found");
            return;
        }

        AssignUiMethods();
    }
	
	void AssignUiMethods ()
    {
        logIn_Button.onClick.AddListener(Authenticate_Button);
        register_Button.onClick.AddListener(Register_Button);
    }

    void Authenticate_Button()
    {
        if (userName_Input.text == string.Empty && userName_Input.text == "")
        {
            Debug.LogError("Authenticate_Button | UserName Cannot be empty");
            return;
            //Add Pw case
        }
        else
        {
            sparkManager.userName = userName_Input.text;
            sparkManager.passWord = passWord_Input.text;
            sparkManager.TEST_AuthenticatePlayer();
        }
    }

    void Register_Button()
    {
        if (userName_Input.text == string.Empty && userName_Input.text == "")
        {
            Debug.LogError("Register_Button | UserName Cannot be empty");
            return;
            //Add Pw case
        }
        else
        {
            sparkManager.userName = userName_Input.text;
            sparkManager.passWord = passWord_Input.text;
            sparkManager.TEST_RegisterPlayer();
        }
    }

    public void Toggle_LoginUi()
    {
        bool enableLogInUi = !LogIn_UiHolder.activeInHierarchy;
        LogIn_UiHolder.SetActive(enableLogInUi);
        //Debug.LogWarning("Toggle_LoginUi");

        //*
        bool enableGameUi = !Game_UiHolder.activeInHierarchy;
        Game_UiHolder.SetActive(enableGameUi);
        //*/
    }

    public void SubmitScore()
    {
        int score =0;
        score = int.Parse(score_input.text);

        SparkManager.Instance().SubmitScoreEvent(score);
    }
}
