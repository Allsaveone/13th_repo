﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_AlertHandler : MonoBehaviour {

    public GameObject alertPrefab;
    public LayoutGroup layOut;
    public Scrollbar scrollBar;

    public List<GameObject> alerts;

    IEnumerator Start()
    {
        scrollBar.onValueChanged.AddListener(OnValueChanged);

        for (int i = 0; i < 20; i++)
        {
            yield return new WaitForSeconds(0.1f);
            CreateAlert(i, "Test" + i);
        }
    }

    public void CreateAlert(int index, string alertText, Image alertIcon = null)
    {
        UI_Alert alert = GameObject.Instantiate(alertPrefab,Vector3.zero,Quaternion.identity).GetComponent<UI_Alert>();
        alert.SetAlert(this,index, alertText);//Icon???
        alert.transform.SetParent(layOut.transform, false);

        alerts.Add(alert.gameObject);
    }

    public void OnValueChanged(float value)
    {
        //Debug.Log("!!!!!" + value);
    }

    public void DeleteAlert(int index)
    {
        Debug.Log(index);

        GameObject alert = alerts[index];
        //alerts.RemoveAt(index);
        alerts[index] = null;
        //Destroy(alert);
        alert.SetActive(false);

        layOut.gameObject.SetActive(false);
        layOut.gameObject.SetActive(true);//resize content group when deleting from bottom up.
    }
}

