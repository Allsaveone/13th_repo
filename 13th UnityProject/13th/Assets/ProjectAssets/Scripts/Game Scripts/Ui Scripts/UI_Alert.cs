﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Alert : MonoBehaviour {

    UI_AlertHandler handler;

    public Text alertText;
    public Image alertIcon;
    public Button deleteButton;

    public int alertIndex;

    public void SetAlert(UI_AlertHandler handler, int alertIndex, string alertText, Image icon = null  )
    {
        this.handler = handler;
        this.alertIndex = alertIndex;
        this.alertText.text = alertText;

        if (icon)
        {
            this.alertIcon = icon;
        }
    }

    public void DismissAlert()
    {
        handler.DeleteAlert(alertIndex);
    }
}
