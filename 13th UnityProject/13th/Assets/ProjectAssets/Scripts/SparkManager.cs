﻿///13th SparkManager
///David Oneill
///4/2/2017
using UnityEngine;
using UnityEngine.Networking;

using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

using System.Globalization;
using System.Threading;
using System.IO;
using System.Text;

using GameSparks.Core;
using GameSparks.Api.Messages;
using GameSparks.Api.Requests;
using GameSparks.Api.Responses;
using System.Net;
using UnityEditor;
using System.Reflection;

public class SparkManager : MonoBehaviour {

    [Header("LogIn Variables")]
    public string userName;
    public string passWord;

    [Header("PlayerDetails")]
    public PlayerDetails playerDetails;

    [Header("GameState")]
    public bool isPaused = false;//This probably be a separate ApplicationManager Class.
    public bool gameSparksAvailable;
    [HideInInspector]
    public GameSparksUnity gs_Unity;

    [Header("TESTING")]
    public bool doTest = false;

    /// <summary>
    /// PlayerDetails holds local playerInfo on client device.
    /// </summary>
    [Serializable]
    public class PlayerDetails
    {
        public string displayName;
        [HideInInspector]
        public string userEmail;
        public string userId;
        //public string[] virtualGoods;
        //public int score;
        //public int level;

        public PlayerDetails(string displayName, string userId, GSData responseData)
        {
            this.displayName = displayName;
            this.userId = userId;

            if (responseData.GetStringList("Bundles") != null)
            {
                List<string> bundleList = responseData.GetStringList("Bundles");

                if (AssetBundleManager.Instance)
                {
                    AssetBundleManager.Instance.StartCoroutine(AssetBundleManager.Instance.RetrieveAssetBundles(bundleList));
                }
            }

            #region Parsing GsData<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            /*
            if (responseData.GetGSDataList("dataArray") != null)
            {
                List<GSData> dataList = responseData.GetGSDataList("dataArray");

                foreach (var data in dataList)
                {
                    Debug.Log(data.GetInt("id"));

                }
            }

            
            if (responseData.GetString("Key") != null) // balance might not be present if the player has just registered. so in this case, we  set the balance to zero
            {
                Debug.Log("PlayerDetails| Key:" + responseData.GetString("Key"));
            }

            if (responseData.GetGSDataList("dataArray") != null)
            {
                List<GSData> dataList = responseData.GetGSDataList("dataArray");

                foreach (var data in dataList)
                {
                    Debug.Log(data.GetInt("id"));

                }
            }

            if (responseData.GetGSDataList("dataArray") != null)
            {
                var dataList = responseData.GetGSDataList("dataArray");

                Debug.LogWarning("dataArray" + dataList.ToString());

                foreach (var item in dataList)
                {
                    Debug.Log(item.GetNumber("id"));
                }
            }

            //Debug.Log("PlayerDetails| displayName:" + displayName);
            
            if (responseData.GetString("email") != null) // balance might not be present if the player has just registered. so in this case, we  set the balance to zero
            {
                this.userEmail = responseData.GetString("email");
                Debug.Log("PlayerDetails| Email:" + userEmail);
            }

            if (responseData.GetInt("score").HasValue) // balance might not be present if the player has just registered. so in this case, we  set the balance to zero
            {
                this.score = responseData.GetInt("score").Value;
                Debug.Log("PlayerDetails| score:" + this.score);
            }
            else
            {
                this.score = 0;
            }

            if (responseData.GetInt("level").HasValue) // balance might not be present if the player has just registered. so in this case, we  set the balance to zero
            {
                this.level = responseData.GetInt("level").Value;
                Debug.Log("PlayerDetails| Level:" + this.level);
            }
            else
            {
                this.level = 0;
            }
            /*
			// we need to check if the player has an VGs before constructing the list
			if(responseData.GetStringList("virtualGoods")  != null)
			{
				virtualGoods = responseData.GetStringList("virtualGoods").ToArray();
			}

            if (responseData.GetString("someString") != null) // balance might not be present if the player has just registered. so in this case, we  set the balance to zero
            {
                //string temp = responseData.GetString("currentMask").Replace("mask","");
                //Debug.Log("PlayerDetails| currentMask:" + temp);
                //this.currentMask = int.Parse(temp);
            }
            */
#endregion Parsing GsData<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        }
    }

    #region Gamesparks SingleTon <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    public static SparkManager instance;

    public static SparkManager Instance()
    {
        if (instance != null)
        {
            if (!GS.Available && GS.Instance == null)
            {
                Debug.LogError("GSM| Connection Lost, attempting to Reconnect...");
                
                GS.Reconnect();
                return null;
            }

            return instance;
        }
        Debug.LogError("GSM| GameSparks Not Initialized...");
        return instance;
    }

    /// <summary>
    /// ResetGSWithNewApiKey
    /// Shut down the sdk, change the Settintgs, Spin the sdk back up...
    /// </summary>
    public void ResetGSWithNewApiKey()
    {
        //GS.ShutDown();
        DisableSDK();

        GameSparksSettings.ApiKey = "x318542RvlTj";
        GameSparksSettings.ApiSecret = "ADp0XEymkpwZBcTV2twvpK79ghY1n18v";
        GameSparksSettings.Credential = "Custom";
        GameSparksSettings.DebugBuild = true;

        StartCoroutine(ResetGameSparks(1.0f));
    }

    /// <summary>
    /// ResetGameSparks
    /// </summary>
    /// <returns></returns>
    IEnumerator ResetGameSparks(float wait)
    {
        yield return new WaitForSeconds(wait);

        while (GS.Available)
        {
            yield return new WaitForSeconds(0.1f);
        }

        Destroy(this.GetComponent<GameSparksUnity>());
        this.gameObject.AddComponent<GameSparksUnity>();
    }

    void Awake()
    {
        if (instance == null)
        {
            //GameSparksSettings.ApiKey = "t313402b2WV0";
            //GameSparksSettings.ApiSecret = "Gi1Ybt8UWWSRXvJpX7oZ5jnucGSl5q96";
            //GameSparksSettings.DebugBuild = true;

            PlayerPrefs.DeleteAll();//TEST clear! - use GS_DONT_USE_PLAYERPREFS scriptDefinedSymbols  <---------------------//////////////////////////////////////////////////////////////////////////////////////////////////////
            
            //Set Auth token of another player to log in as them, if token is valid
            //PlayerPrefs.SetString("gamesparks.authtoken", "35b21a8f-6a64-40ab-b02b-1a92f8e6f41b");

            Debug.Log("GSM| Singleton Initialized...");
            instance = this;
            DontDestroyOnLoad(this.gameObject);
            this.gameObject.AddComponent<GameSparksUnity>();


            AssignMessageListeners();
        }
        else
        {
            //There can be only one!
            Debug.Log("GSM| Removed Duplicate...");
            Destroy(this.gameObject);
        }        
    }

    #endregion Gamesparks SingleTon <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    #region Init <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    void Start()
    {     
        GS.GameSparksAvailable += GameSparksAvailable;
        Debug.Log("GS.Authenticated: " + (GS.Authenticated ? "AUTHENTICATED" : "NOT AUTHENTICATED"));

        //GS.Instance.DurableQueueRunning = false;

        //Test Auth
        //Invoke("TEST_AuthenticatePlayer", 2f);
        Debug.Log("GSM| ScriptingDefineSymbols: " + PlayerSettings.GetScriptingDefineSymbolsForGroup(BuildTargetGroup.Standalone));

        //GS.Instance.GameSparksAuthenticated 

        //StartCoroutine(AvailableCheck());
    }

    //Testing for an Odd Ticket......
    IEnumerator AvailableCheck()
    {
        float t = 0f;
        while (!GS.Available)
        {
            t += Time.deltaTime;
            if (t > 20f) Debug.LogError("Wut?");// RestartGame();

            GS.Reset();
            GS.Disconnect();
            GS.Reconnect();
            yield return new WaitForSeconds(4f);
            Debug.LogError("yTho?");
        }
    }

    private void GameSparksAvailable(bool available)
    {
        if (available)
        {
            Debug.LogWarning(">>>>> GameSparks Available/Connected <<<<<");
            gameSparksAvailable = true;
            //HandleDurableRequests();
            //TESTING Toggle_LoginUi
            GameObject.Find("UICanvas").GetComponent<BasicLogIn>().Toggle_LoginUi();

            //Switch Configs
            //switchConfig = true;
            //if (switchConfig)ResetGSWithNewApiKey();
            //Test Auth
            //TEST_AuthenticatePlayer();  
        }
        else
        {           
            Debug.LogWarning(">>>>> GameSparks Unavailable/Disconnected <<<<<");
            gameSparksAvailable = false;
        }
    }

    public void DisableSDK()
    {
        Debug.LogWarning(">>>>> ShutDown <<<<<");
        GS.ShutDown();
    }

    #endregion Init <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    #region Registraion/Authentication <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    /// <summary>
    /// AuthError
    /// </summary>
    /// <param name="error"></param>
    void AuthError(string error)
    {
        Debug.Log("GSM| AuthError" + error);

        if (error.Contains("timeout"))
        {
            Debug.LogError("GSM| AuthError TimeOut");
        }

        if (error.Contains("UNRECOGNISED"))
        {
            Debug.LogError("GSM| AuthError UNRECOGNISED");
        }

        /* Hmm.... may be wiser to handle the whole response object..................
        if (authReponse.Errors.JSON != null)
        {
            GSData errorData = authReponse.Errors;

            if (errorData.GetString("DETAILS") == "UNRECOGNISED")
            {
                Debug.Log("authReponse.Error | : UNRECOGNISED");
            }
        }
        */
    }

    /// <summary>
    /// OnPlayerAuthenticated- Delegate method for handling successful authentication
    /// </summary>
    /// <param name="playerDetails"></param>
    public delegate void OnPlayerAuthenticated(PlayerDetails playerDetails);

    /// <summary>
    /// RegisterPlayer
    /// </summary>
    /// <param name="displayName"></param> Set new user displayname
    /// <param name="password"></param> Set new user password
    /// <param name="onPlayerRegSuccess"></param> success callback
    /// <param name="onRegError"></param> error callback
    /// <param name="emailAddress"></param> OPTIONAL - pass in email address to be stored against the player
    public void RegisterPlayer(string displayName, string password, OnPlayerAuthenticated onPlayerRegSuccess, OnError onRegError, string emailAddress = null)
    {
        GSRequestData emailData = new GSRequestData();
        Debug.Log("GSM| Attempting to RegisterPlayer With GameSparks....");

        if (emailAddress == null)
        {
            Debug.Log("GSM| No Email provided on Auth....");
        }
        else
        {
            emailData.Add("email", emailAddress);
        }

        new RegistrationRequest()
         .SetDisplayName(displayName)
            .SetUserName(displayName)
            .SetPassword(password)
            .SetScriptData(emailData)
            //.SetSegments(segData)
            .Send((regResponse) =>
            {

                if (!regResponse.HasErrors && onPlayerRegSuccess != null)
                {
                    Debug.LogWarning("GSM| Registration Succesful...");
                    onPlayerRegSuccess(new PlayerDetails(regResponse.DisplayName, regResponse.UserId, regResponse.ScriptData));
                }
                else
                {
                    //Debug.LogError("GSM| Registration ERROR-" + regResponse.Errors.JSON);

                    if (!(bool)regResponse.NewPlayer)  // If an Existing Player, use Authentication instead                            
                    {
                        AuthenticatePlayer(displayName, password, onPlayerRegSuccess, onRegError);
                        return;
                    }

                    if (onRegError != null)
                    {
                        onRegError("GSM| Registration ERROR-" + regResponse.Errors.JSON);
                    }
                }
            });
    }

    /// <summary>
    /// Authenticate_Player
    /// </summary>
    /// <param name="displayName"></param>//user display name
    /// <param name="password"></param>//user password
    /// <param name="onPlayerAuthSuccess"></param> success callback
    /// <param name="onAuthError"></param>error callback
    public void AuthenticatePlayer(string displayName, string password, OnPlayerAuthenticated onPlayerAuthSuccess, OnError onAuthError)
    {
        Debug.Log("GSM| Attempting to Authenticate_Player With GameSparks....");

        new AuthenticationRequest()
            .SetUserName(displayName)
            .SetPassword(password)
            .Send((authReponse) => {

                if (!authReponse.HasErrors && onPlayerAuthSuccess != null)
                {
                    Debug.Log("GSM| Authenticating Player /n " + authReponse.DisplayName);  // Log errors 
                    onPlayerAuthSuccess(new PlayerDetails(authReponse.DisplayName, authReponse.UserId, authReponse.ScriptData));

                    GSData responseData = authReponse.ScriptData;                  
                }
                else
                {
                    Debug.Log("GSM| Error Authenticating Player /n " + authReponse.Errors.JSON);  // Log errors 
                    if (onAuthError != null)
                    {
                        onAuthError(authReponse.Errors.JSON); 
                    }
                }
            });
    }

    /// <summary>
    /// PlayerAuthenticated
    /// </summary>
    /// <param name="playerDetails"></param> class containing player info
    void PlayerAuthenticated(PlayerDetails playerDetails)
    {
        Debug.Log("GSM| PlayerAuthenticated: DisplayName:" + playerDetails.displayName + "\n" + "UserID:" + playerDetails.userId);
        this.playerDetails = playerDetails;

        //TESTING Toggle_LoginUi
        GameObject.Find("UICanvas").GetComponent<BasicLogIn>().Toggle_LoginUi();

        SceneHandler sHandler =  GameObject.Find("SceneHandler").GetComponent<SceneHandler>();
        //sHandler.StartCoroutine(sHandler.LoadSceneAsnc("UI_Development"));   

        //DisableSDK();

        //Test Method calls............
        if (doTest)
        {
            Debug.LogWarning("DoTest-PlayerAuthenticated");

            /*
            Debug.LogWarning(GS.GSPlatform.AuthToken);
            Debug.LogWarning(GS.GSPlatform.ApiKey);
            Debug.LogWarning(GS.GSPlatform.ApiSecret);
            Debug.LogWarning(GS.GSPlatform.UserId);
            Debug.LogWarning(GS.GSPlatform.ApiStage);
            */

            DoTestStuff();         
        }

    }

    /// <summary>
    /// EndPlayerSession
    /// Public acessor for Ending the players gameSparks Session.
    /// </summary>
    public void EndPlayerSession()
    {
        EndSession(EndSessionSuccess, OnEndSessionError);
    }

    /// <summary>
    /// OnEndSessionSuccess
    /// delegate for successfully ending a session on GameSparks.
    /// </summary>
    public delegate void OnEndSessionSuccess(float duration );

    /// <summary>
    /// PlayerEndSessionSuccess
    /// </summary>
    /// <param name="duration"></param>
    void EndSessionSuccess(float duration)
    {
        Debug.Log("GSM | Session Ended Duration: " + (duration/1000) + " seconds");
    }

    void OnEndSessionError(string error)
    {
        Debug.Log("GSM | Ended Session ERROR " + error);
    }

    /// <summary>
    /// EndSession
    /// </summary>
    /// <param name="onSuccess"></param>
    /// <param name="onError"></param>
    public void EndSession(OnEndSessionSuccess onSuccess, OnError onError)
    {
        Debug.Log("GSM | sending EndSession"); //Added Disconnect to EndSessionRequest cloud code.

        new EndSessionRequest()
          .Send((endSessionResponse) => {

              if (!endSessionResponse.HasErrors)
              {
                  if (onSuccess != null)
                  {
                      //float duration = ((endSessionResponse.SessionDuration == null) ? 0 : endSessionResponse.SessionDuration);
                      onSuccess((float)endSessionResponse.SessionDuration);
                  }
              }
              else
              {
                  if (onError != null)
                  {
                      onError(endSessionResponse.Errors.JSON);
                  }

                  //Debug.Log(endSessionResponse.Errors.JSON);
              }
          });
    }

    #endregion Registraion/Authentication <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    #region Virtual Goods/Currency <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    /// <summary>
    /// Purchase a VirtualGood from Gamesparks
    /// </summary>
    /// <param name="virtualGood"></param>The shortcode of the virtualGood
    /// <param name="quantity"></param> how many should be bought
    /// <param name="currency"></param> The shortcode of the currency to pay with
    public void Purchase_VirtualGood(string virtualGood, int quantity, string currency, OnBuyVirtualGood onBuyVgSuccess, OnError onBuyVgError)
    {
        new BuyVirtualGoodsRequest()
        .SetShortCode(virtualGood)
        .SetQuantity(quantity)
        .SetCurrencyShortCode(currency)
        .Send(On_BuyGoodsSuccess, On_BuyGoodsError);//buyVgResponse)
        /*     
             => {

            if (!buyVgResponse.HasErrors)
            {
                if (onBuyVgSuccess != null)
                {
                    #region Testing Properties for Vgs  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<THIS IS NOT A GOOD WAY<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
                    if (buyVgResponse.ScriptData.GetGSDataList("ItemProperties") != null)
                    {
                        List<GSData> itemProperties = buyVgResponse.ScriptData.GetGSDataList("ItemProperties");

                        if (itemProperties != null)
                        {
                            foreach (var property in itemProperties)
                            {
                                string itemSlot = property.GetGSData("ItemProperty").GetString("ItemSlot");

                                if (itemSlot != null)
                                {
                                    //Its an Equipable item!
                                    Debug.Log("itemSlot:" + itemSlot);
                                }
                                //Debug.LogError(property.GetGSData("ItemProperty").GetString("ItemSlot") );
                            }
                        }
                    }
                    #endregion Testing Properties for Vgs  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
                    //Really.... Items should be stored and handled on the server Meta/Runtime collection implimentation.... item docs would contain bundles details/ asset name etc etc etc
                    onBuyVgSuccess(buyVgResponse.BoughtItems);
                }
            }
            else
            {
                if (onBuyVgError != null)
                {
                    onBuyVgError(buyVgResponse.Errors.JSON);
                }
            }

        });
        */
    }

    public delegate void BuyGoodsSuccess(BuyVirtualGoodResponse resp);

    ///*
    public void On_BuyGoodsSuccess(BuyVirtualGoodResponse resp)
    {
        
        Debug.LogError(resp.JSONString);
    }

    public delegate void BuyGoodsError(BuyVirtualGoodResponse resp);

    public void On_BuyGoodsError(BuyVirtualGoodResponse resp)
    {
        Debug.LogError(resp.JSONString);
    }
    //*/

    /// <summary>
    /// OnBuyVirtualGood-Delegate method for handeling successful purchases
    /// </summary>
    /// <param name="boughtItems"></param>
    public delegate void OnBuyVirtualGood(GSEnumerable<BuyVirtualGoodResponse._Boughtitem> boughtItems);

    /// <summary>
    /// VirtualGoodPurchased
    /// </summary>
    /// <param name="boughtItems"></param>Items successfully purchased
    public void VirtualGoodPurchased(GSEnumerable<BuyVirtualGoodResponse._Boughtitem> boughtItems)
    {
        foreach (var item in boughtItems)
        {
            Debug.Log("GSM| VirtualGoodPurchased: " + item.ShortCode + " : " + item.Quantity);
        }
    }

    /// <summary>
    /// VirtualGoodError
    /// </summary>
    /// <param name="error"></param> Handles Errors encountered when purchasing VirtualGoods
    void VirtualGoodError(string error)
    {
        Debug.Log("GSM| VirtualGoodError: " + error);

        if (error.Contains("INSUFFICIENT_FUNDS"))
        {
            Debug.Log("GSM| VirtualGoodError: INSUFFICIENT_FUNDS");
        }
    }

    #endregion Currency <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    #region Messages <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    /// <summary>
    /// Wrapper class to register listeners for Gamesparks messages.
    /// </summary>
    private void AssignMessageListeners()
    {
        ScriptMessage.Listener += ScriptMessageHandler;
        SessionTerminatedMessage.Listener += SessionTerminatedHandler;

        AchievementEarnedMessage.Listener += AchievementMessageHandler;

        NewHighScoreMessage.Listener += HighScoreMessageHandler;

        MatchFoundMessage.Listener += MatchFoundMessageHandler;
        MatchNotFoundMessage.Listener += MatchNotFoundMessageHandler;
        MatchUpdatedMessage.Listener += MatchUpdatedMessageHandler;

        UploadCompleteMessage.Listener += UploadCompleteMessageHandler;

        ChallengeTurnTakenMessage.Listener += TurnTakenMessageHandler;
    }

    /// <summary>
    /// Send a Message to another Player
    /// </summary>
    /// <param name="playerId"></param> The target player
    /// <param name="messageToSend"></param> The data payload
    public void Send_PlayerMessage(string playerId, GSRequestData messageToSend)
    {
        new LogEventRequest()
            .SetEventKey("SendPlayerMessage")//Your event short code.
            .SetEventAttribute("PlayerId", playerId)//the player you want to send to
            .SetEventAttribute("MessageData", messageToSend)//the string you want to send
            .SetDurable(false)
            .Send((response) =>
            {
                if (!response.HasErrors)
                {
                    Debug.Log("Message:" + messageToSend + "-sent from Unity client to " + playerId);
                }
                else
                {

                    Debug.Log(response.Errors.JSON);
                }
            });
    }

    /// <summary>
    /// Generic ScriptMessage Listener - filtered by ExtCode
    /// </summary>
    /// <param name="_message"></param>
    void ScriptMessageHandler(ScriptMessage _message)
    {
        string extCode = _message.ExtCode;

        switch (extCode)
        {
            case "PlayerMessage":

                Debug.Log("GSM |Message recieved from: " + _message.Data.GetString("sender"));
                Debug.Log("GSM |SenderId: " + _message.Data.GetString("senderId"));
                Debug.Log("GSM |Message: " + _message.Data.GetString("textBody"));
                Debug.Log("GSM |Message: " + _message.MessageId);

                break;
            default:
                Debug.LogError("GSM | ERROR-ScriptMessage Code-" + extCode + "- UNKNOWN");
                break;
        }
    }

    private void SessionTerminatedHandler(SessionTerminatedMessage _message)
    {
        Debug.LogWarning("GSM | SessionTerminated: " + _message.JSONString);
        //Handle LogOut Here...................

        //TESTING Toggle_LoginUi
        GameObject.Find("UICanvas").GetComponent<BasicLogIn>().Toggle_LoginUi();
    }

    public void AchievementMessageHandler(AchievementEarnedMessage _message)
    {
        Debug.Log("GSM| AWARDED ACHIEVEMENT \n " + _message.AchievementName);
    }

    public void HighScoreMessageHandler(NewHighScoreMessage _message)
    {
        Debug.Log("GSM| HIGH SCORE \n " + _message.JSONString);
    }

    public void MatchFoundMessageHandler(MatchFoundMessage _message)
    {
        Debug.Log("GSM| Match Found \n " + _message.JSONString + " \n MatchID :"  + _message.MatchId);

        /*     
        GSRequestData requestData = new GSRequestData(data);
        //Send with logEvent to store on playerDoc

        MatchFoundMessage spoofMsg = new MatchFoundMessage(data);
        Debug.Log("GSM| Match Found -SpoofMessage \n " + spoofMsg.JSONString);
        */
    }

    public void MatchNotFoundMessageHandler(MatchNotFoundMessage _message)
    {
        Debug.Log("GSM| Match Not Found \n " + _message.JSONString);
    }

    public void MatchUpdatedMessageHandler(MatchUpdatedMessage _message)
    {
        Debug.Log("GSM| Match Updated \n " + _message.JSONString);
    }

    public void UploadCompleteMessageHandler(UploadCompleteMessage _message)
    {
        Debug.Log("GSM| Upload Complete! \n " + _message.JSONString);
    }

    //Challenge Messages
    public void ChallengeStartedMessageHandler(ChallengeStartedMessage _message)
    {
        ChallengeStartedMessage._Challenge startedChallenge = _message.Challenge;
    }

    public void ChallengeAcceptedMessageHandler(ChallengeAcceptedMessage _message)
    {
        ChallengeAcceptedMessage._Challenge acceptedChallenge = _message.Challenge;
        GSEnumerable<ChallengeAcceptedMessage._Challenge._PlayerDetail> acceptedPlayers = acceptedChallenge.Accepted;

        foreach (var player in acceptedPlayers)
        {
            Debug.Log(player.Id);
        }
    }

    void TurnTakenMessageHandler(ChallengeTurnTakenMessage _message)
    {
        Debug.LogError(_message);
    }

    #endregion MESSAGES<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    #region Application Methods (Unity ONLY) <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    /// <summary>
    /// OnAppFocus
    /// Public Delegate for other classes to listen for - Neat patern
    /// </summary>
    /// <param name="hasFocus"></param>
    public delegate void OnAppFocus(bool hasFocus);
    public OnAppFocus OnFocusListener;


    /// <summary>
    /// OnApplicationFocus Callback -STANDALONE
    /// </summary>//
    /// Detects when the app enters and exits background mode
    /// <param name="hasFocus"></param>The focus state of the application
    public void OnApplicationFocus(bool hasFocus)
    {
        if (Application.runInBackground == true)
        {
            //return;
        }

        isPaused = !hasFocus;

        if (OnFocusListener != null)
            OnFocusListener(isPaused);

        if (isPaused)
        {
            //EndPlayerSession();
            //DisableSDK();
            Debug.Log("GSM | OnApplicationFocus-hasFocus: Background Mode");
        }
        else
        {
            Debug.Log("GSM | OnApplicationFocus-hasFocus: Foreground Mode");
            //StartCoroutine(ResetGameSparks(0.1f));
        }
    }

    /// <summary>
    /// OnApplicationPause -ANDROID-STANDALONE
    /// </summary>
    ///Detects when the app enters and exits background mode NB- Affected by RunInBackgroundMode.
    /// <param name="pauseStatus"></param>
    void OnApplicationPause(bool pauseStatus)
    {
        if (Application.runInBackground == true)
        {
            //return;
        }

        isPaused = pauseStatus;

        if (isPaused)//Testing...Auto reconnect after returning from background mode
        {
            //Debug.Log("GSM | OnApplicationPause-pauseStatus: PAUSED");
            Debug.LogWarning("GSM | OnApplicationPause-pauseStatus: " + pauseStatus);// + " GS.Available:" + GS.Available);
        }
        else
        {
            Debug.Log("GSM | OnApplicationPause-pauseStatus: UnPAUSED");
        }
    }

    /// <summary>
    /// OnApplicationQuit
    /// Called when .... wait for it.... The Application Quits...
    /// </summary>
    void OnApplicationQuit()
    {
        Debug.Log("GSM | OnApplicationQuit");
    }

    /// <summary>
    /// OnDisable
    /// </summary>
    void OnDisable()
    {
        StopAllCoroutines();
    }

    /// <summary>
    /// OnDestroy
    /// </summary>
    void OnDestroy()
    {
        StopAllCoroutines();
    }

    #endregion Application Methods <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    #region   Downloadables/Uploadables <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    public delegate void OnGetDownloadURLsuccess(string shortCode, string url);

    public void OnGetDownloadURL(string shortCode, string url)
    {
        //Filer by short code? Here or in bundleManager? - BundleManager fro sho
        StartCoroutine(AssetBundleManager.Instance.LoadAssetBundle(shortCode, url));
    }

    public void OnGetDownloadError(string error)
    {
        Debug.LogError("GSM| OnGetDownloadError" + error);
    }

    /// <summary>
    /// GetDownloadableURL
    /// </summary>
    public void GetDownloadableURL(string downloadShortCode, OnGetDownloadURLsuccess onSuccess, OnError onError)
    {
        new GetDownloadableRequest()
        .SetShortCode(downloadShortCode)
        .Send((response) => {
            if (!response.HasErrors)
            {
                DateTime? lastModified = response.LastModified;
                GSData scriptData = response.ScriptData;

                string shortCode = response.ShortCode;
                var size = response.Size;
                string url = response.Url;

                if (onSuccess != null)
                {
                    onSuccess(shortCode, url);// <----- Need to do Downaloadable handling here
                }

            }
            else
            {
                if (onError != null)
                {
                    onError(response.Errors.JSON);
                }
            }
        });
    }

    public void GetUploadUrlAndUpload(byte[] data = null)
    {
        data = System.Text.Encoding.UTF8.GetBytes("TestTestTestTestTestTestTestTest");
       
        new GetUploadUrlRequest()
       .Send((response) => {

           if (!response.HasErrors)
           {
               Debug.Log(response.JSONString);
               GSData scriptData = response.ScriptData;
               string url = response.Url;

               StartCoroutine(UploadFile(url, data));
           }
           else
           {
               Debug.Log("GSM | GetUploadUrlAndUpload ERROR: " + response.Errors.JSON);
           }
       });
    }

    IEnumerator UploadFile(string url, byte[] bytes)
    {
        var form = new WWWForm();
        form.AddField("somefield", "somedata");
        form.AddBinaryData("file", bytes, "testText", "text");

        WWW w = new WWW(url, form);
        yield return w;

        if (w.error != null)
        {
            Debug.Log("GSM | UploadFile ERROR: " + w.error);
        }
        else
        {
            Debug.Log("GSM | UploadFile: " + w.text);
        }
    }

    /// <summary>
    /// CustomCallback
    /// Hits an endpoint on Gs
    /// EG method
    /// </summary>
    /// <param name="url"></param>
    /// <returns></returns>
    IEnumerator CustomCallback(string url)
    {
        //https://{apiKey}.{stage}.gamesparks.net/callback/{apiKey}/{credential}/{credentialSecret}
        url = "https://t313402b2wv0.preview.gamesparks.net/callback/t313402b2WV0/Custom/ADp0XEymkpwZBcTV2twvpK79ghY1n18v";

        Dictionary<string, string> dict = new Dictionary<string, string>();
        dict.Add("someKey", "WOOTINI!!!");

        WWWForm form = new WWWForm();
        form.AddField("someKey", "WOOTINI!!!");

        UnityWebRequest www = new UnityWebRequest();
        www.SetRequestHeader("Content-Type", "application/json");

        using (www = UnityWebRequest.Post(url, dict ))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.LogError(www.responseCode + "/" + www.error);
            }
            else
            {
                Debug.LogWarning( www.downloadHandler.text);
            }
        }
    }

    #endregion Downloadables<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    #region  LogEvents <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


    void GenericLogEvent()
    {
        new LogEventRequest()
           .SetEventKey("GenericLogEventShortCode")
           .SetEventAttribute("GenericAttributeShortCode", "someValue")
           .Send((response) =>
           {
               if (!response.HasErrors)
               {
                   Debug.Log("GSM | GenericLogEvent Success: " + response.JSONString);
               }
               else
               {
                   Debug.LogError("GSM | GenericLogEvent Error: " + response.Errors.JSON);
               }
           });
    }


    public void GenericLogEvent_DoTest()
    {
        new LogEventRequest()
           .SetEventKey("DoTest")
           .Send((response) =>
           {
               if (!response.HasErrors)
               {
                   Debug.Log("GSM | GenericLogEvent Success: " + response.JSONString);

                   //string userID = response.ScriptData.GetGSData("player_Data").GetString("id");
                   //Debug.LogError(userID);
               }
               else
               {
                   Debug.LogError("GSM | GenericLogEvent Error: " + response.Errors.JSON);
               }
           });
    }

    public void SubmitScoreEvent(int score)
    {
        //Debug.Log("score " + score * 100);
        //int covertedScore = Mathf.FloorToInt(score * 100); //cuz we cant send floats.....
        //Debug.Log("acceptableValue " + covertedScore);

        new LogEventRequest()
            //.Set_Score(score)
            .SetEventKey("SubmitScore")
            .SetEventAttribute("Score", score)
            .SetDurable(true)
            .Send((scoreResponse) => 
            {
                if (!scoreResponse.HasErrors)
                {
                    Debug.Log("GSM | SubmitScoreEvent Success: " + scoreResponse.JSONString);
                    var scoreData = scoreResponse.ScriptData.GetNumber("scorePosted");
                    Debug.Log("GSM | SubmitScoreEvent scorePosted: " + scoreData);
                }
                else
                {
                    Debug.LogError("GSM | SubmitScoreEvent Error: " + scoreResponse.Errors.JSON);
                }
            });
    }
    
    //Runtime collection Events.
    /// <summary>
    /// Accessor for QueryEvent
    /// </summary>
    public void GetOnlinePlayers()
    {
        QueryDataBase("findOnlinePlayers", QuerySuccess, QueryError, "");
    }

    public delegate void OnQuerySuccess(GSData resultData);

    void QuerySuccess(GSData resultData)
    {
        Debug.Log("GSM | QuerySuccess: " + resultData.JSON);

        List<GSData> results = resultData.GetGSDataList("searchResults");

        if (results.Count > 0)
        {
            foreach (var result in results)
            {
                string resultName = result.GetString("displayName");

                //string oid = result.GetGSData("_id").GetString("$oid");
                //Debug.Log(oid);

                if (resultName == playerDetails.displayName)
                {
                    Debug.Log("Ignore my own entry in the results....");
                }
                else
                {
                    Debug.Log(result.GetString("displayName"));
                }
            }
        }
        else
        {
            Debug.Log("No search results");
        }
    }

    /// <summary>
    /// QueryError
    /// </summary>
    /// <param name="error"></param>
    void QueryError(string error)
    {
        Debug.LogError("GSM | QueryError" + error);
    }

    /// <summary>
    /// Query GS NoSQL Database.
    /// </summary>
    /// <param name="query"></param> The string value to be passed to cloud code to initialise the search eg - findOnlinePlayers
    /// <param name="queryKey"></param> The string value to be passed to the search eg - {"displayName" : queryKey} //
    /// 
    void QueryDataBase(string query, OnQuerySuccess onSuccess, OnError onError, string queryKey = "")
    {
        new LogEventRequest()
            .SetEventKey("QueryDataBase")
            .SetEventAttribute("Query", query)
            .SetEventAttribute("QueryKey", queryKey)
            .Send((queryResponse) => {

                if (!queryResponse.HasErrors)
                {
                    if (onSuccess != null)
                    {
                        onSuccess(queryResponse.ScriptData);
                    }
                }
                else
                {
                    if (onError != null)
                    {
                        onError(queryResponse.Errors.JSON);
                    }
                }
            });
    }

    #endregion <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    #region   MatchMaking<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    public delegate void MatchMakingError(string error);

    void OnMatchMakingError(string error)
    {
        Debug.LogError("GSM | OnMatchMakingError: " + error);
    }

    public void Test_MatchMaking()
    {
        SimpleMatchMaking("MatchTest", OnMatchMakingError);
    }

    public void SimpleMatchMaking(string matchShortCode, MatchMakingError onError)
    {
        new MatchmakingRequest()
        //.SetAction("cancel") //Cancel any ongoing MatchMaking
        //.SetCustomQuery() //The query that will be applied to the PendingMatch collection
        //.SetMatchData() //A JSON Map of any data that will be associated to the pending match
        //.SetMatchGroup() //Optional. Players will be grouped based on the distinct value passed in here, only players in the same group can be matched together - Factions etc
        .SetMatchShortCode(matchShortCode) //Match short code
        //.SetParticipantData() // JSON Map of any data that will be associated to this user in a pending match
        .SetSkill(0) //The skill of the player looking for a match
        .Send((response) => {

            if (!response.HasErrors)
            {
                Debug.LogWarning("GSM | SimpleMatchMaking Sent: " + response.JSONString);
            }
            else
            {
                if (onError != null)
                {
                    onError(response.Errors.JSON);
                }
            }
        });
    }

    #endregion   MatchMaking<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    #region   Player <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    public void AccountDetails()
    {
        new AccountDetailsRequest()
       .Send((response) => {
           if (!response.HasErrors)
           {
               Debug.Log("AccountDetails" + response.JSONString);
               ParseJson(response.ScriptData.JSON);
               GSData currencyData = response.Currencies;
               Debug.LogWarning("GSM | AccountDetailsRequest- Player Credits: " + (double) currencyData.GetNumber("Credits"));
           }
           else
           {
               Debug.Log("AccountDetails ERROR" + response.Errors.JSON);

           }
       });
    }

    public void JoinTeam()
    {
        new JoinTeamRequest()
            .SetTeamId("GsTeam")
            .Send((response) => {
                if (response.HasErrors)
                {
                    if (response.Errors.GetString("team") == "INVALID")
                    {
                        Debug.Log("Unable to connect to chat server");
                    }
                    else if (response.Errors.GetString("teamType") == "MAX_MEMBERSHIP_REACHED")
                    {
                        Debug.Log("MAX_MEMBERSHIP_REACHED");
                    }
                }
                else
                {
                    Debug.LogWarning(response.JSONString);
                }
            });
    }

    //"GsTeam"
    public void GetTeam(string teamId)
    {
        new GetTeamRequest()
            .SetTeamId(teamId)
            .Send((response) => {
                if (response.HasErrors)
                {
                    Debug.LogError(response.Errors.JSON);
                }
                else
                {
                    GSEnumerable<GetTeamResponse._Team> teams = response.Teams;

                    foreach (var team in teams)
                    {
                        Debug.Log(team.TeamId);
                        Debug.Log(team.TeamName);
                        Debug.Log(team.TeamType);

                        GSEnumerable<GetTeamResponse._Team._Player> members = team.Members;

                        foreach (var member in members)
                        {
                            Debug.Log(member.DisplayName);
                        }
                    }
                }
            });
    }

    //"Squad"
    void GetTeams(string teamID)
    {
        List<string> teamShortCodes = new List<string>();
        teamShortCodes.Add(teamID);

        new GetMyTeamsRequest()
            .SetOwnedOnly(false)
            .SetTeamTypes(teamShortCodes)
            .Send((response) => {

                if (!response.HasErrors)
                {
                    Debug.Log("GSM |GetMyTeamsResponse: " + response);


                    GSEnumerable<GetMyTeamsResponse._Team> teams = response.Teams;

                    foreach (GetMyTeamsResponse._Team team in teams)
                    {
                        Debug.Log(team.JSONString);
                        string _teamName = team.JSONData["teamName"].ToString();
                        Debug.Log(_teamName);

                        string _id = team.JSONData["teamId"].ToString();
                        Debug.Log(_id);
                    }
                }

                else
                {
                    Debug.Log("GSM |GetMyTeamsResponse: Error loading Team");

                }
            });
    }

    #endregion   Player <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    #region   Challenge Methods<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    //Test accessor
    public void CreateChallenge_Button()
    {
        List<string> playerList = new List<string>();
        playerList.Add("59f21651112c29022a9024af");

        CreateChallenege("TestChal", playerList);
    }

    void CreateChallenege(string challengeShortCode, List<string> usersToChallenge)
    {
        GSRequestData data = new GSRequestData();
        data.Add("SomeKey", "someVar");

        new CreateChallengeRequest()
            .SetChallengeShortCode(challengeShortCode)
            .SetMinPlayers(2)
            .SetMaxPlayers(2)
            .SetUsersToChallenge(usersToChallenge)
            //.SetEndTime(System.DateTime.Now.AddSeconds(60))
            //.SetExpiryTime(System.DateTime.Now.AddSeconds(60))
            .SetChallengeMessage("CreateChallenege from Client")
            .SetScriptData(data)
            .SetDurable(true)
            .Send((createResponse) => {
                if (!createResponse.HasErrors)
                {
                    Debug.Log("GSM | CreateChallengeRequest :" + createResponse.JSONString);

                }
                else
                {
                    Debug.LogError("GSM | CreateChallengeRequest ERROR:" + createResponse.Errors.JSON);
                }
            });
    }

    void OnGetChallenegeError(string error)
    {
        Debug.LogError("GSM | OnGetChallenegeError: " + error);
    }

    void GetChallenge(string challengeID, OnError onError)
    {
        new GetChallengeRequest()
        .SetChallengeInstanceId(challengeID)
        //.SetMessage(message)
        .Send((response) => {

            if (!response.HasErrors)
            {
                GetChallengeResponse._Challenge challenge = response.Challenge;
                Debug.LogError(challenge.EndDate.ToString()+ "/" +  DateTime.Now);
                Debug.LogError(DateTime.Parse( challenge.EndDate.ToString()).ToLocalTime() - DateTime.Now);
                //GSData scriptData = response.ScriptData;
                //GSData myData = challenge.ScriptData.GetGSData("someData");
                //int myValue = (int)myData.GetNumber("someValue");

                /*
                List<GSData> myData = challenge.ScriptData.GetGSDataList("gameState");

                foreach (GSData data in myData)
                {
                    GSData pieceData = data.GetGSData("pawn");
                    Debug.Log(pieceData.GetString("name"));
                }
                
                for (int i = 0; i < myData.Count; i++)
                {
                    GSData pieceData = myData[i];
                    Debug.Log(pieceData.ToString());
                }
                */
            }
            else
            {
                if (onError != null)
                {
                    onError(response.Errors.JSON);
                }
            }
        });
    }

    void ListChallenges()
    {
        new ListChallengeRequest().SetShortCode("ChallengeShortCode")
            .SetState("RUNNING")
            .SetEntryCount(10)
            .SetOffset(0)
            .Send((response) => {    

            if (!response.HasErrors)
            {
                Debug.Log(response.JSONString);

                GSEnumerable<ListChallengeResponse._Challenge> challenges = response.ChallengeInstances;
                List<ListChallengeResponse._Challenge> challengeList = new List<ListChallengeResponse._Challenge>();

                //Assign the challenges to a list so we can iterate over them
                foreach (var challenge in challenges)
                {
                    Debug.Log(challenge.ChallengeId);
                    challengeList.Add(challenge);//Add to list
                }

                //Iterate over the list and assign challenege details, challenge 0 to ui element 0, 1 to 1 etc
                for (int i = 0; i < challengeList.Count; i++)
                {
                    /*
                    uiTexts[i].text = challengeList[i].ChallengeId +
                    "\n"
                    + challengeList[i].ChallengeName +
                    "\n"
                    + challengeList[i].ChallengeMessage;
                    */
                }
            }
            else
            {
                Debug.Log(response.Errors.JSON);
            }
        });
    }

    #endregion   Challenge Methods<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    #region   Utility<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    /// <summary>
    /// Delegate method for handling errors.
    /// </summary>
    /// <param name="error"></param>
    public delegate void OnError(string error);

    public static DateTime? ConvertUnixTimeStamp(string unixTimeStamp)
    {
        return new DateTime(1970, 1, 1, 0, 0, 0).AddMilliseconds(Convert.ToDouble(unixTimeStamp)).ToLocalTime();
    }

    public static Vector3 StringToVector3(string sVector)
    {
        // Remove the parentheses
        if (sVector.StartsWith("(") && sVector.EndsWith(")"))
        {
            sVector = sVector.Substring(1, sVector.Length - 2);
        }

        // split the items
        string[] sArray = sVector.Split(',');

        // store as a Vector3
        Vector3 result = new Vector3(
            float.Parse(sArray[0]),
            float.Parse(sArray[1]),
            float.Parse(sArray[2]));

        return result;
    }

    public IDictionary<string, object> ParseJson(string json)
    {
        IDictionary<string, object> parsedSimpleJson = (IDictionary<string, object>)SimpleJson2.SimpleJson2.DeserializeObject(json);

        List<string> objKeys = new List<string>();

        foreach (var item in parsedSimpleJson)
        {
            Debug.Log(item.Key + ":" + item.Value.ToString());

            objKeys.Add(item.Key);
        }

        parsedSimpleJson.Add("keys", objKeys);

        List<string> retKeys = (List<string>)parsedSimpleJson["keys"];

        for (int i = 0; i < retKeys.Count; i++)
        {
            Debug.Log(retKeys[i] + ":" + parsedSimpleJson[retKeys[i]]);
        }

        //Debug.Log(parsedSimpleJson["Bundles"]);

        return parsedSimpleJson;
    }

    public string ObjectToJson(object obj)
    {
        string toSimpleJson = SimpleJson2.SimpleJson2.SerializeObject(obj);
        Debug.Log(toSimpleJson);
        return toSimpleJson;
    }

    #endregion   Utility<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<












        





    #region   TESTING Methods<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    /// <summary>
    /// TEST Auth call
    /// </summary>
    /// 
    public void TEST_AuthenticatePlayer()
    {
        Debug.LogWarning("TEST_AuthenticatePlayer");

        //RegisterPlayer(userName, "", PlayerAuthenticated, AuthError);
        AuthenticatePlayer(userName, "", PlayerAuthenticated, AuthError);
        //DeviceAuth();
    }

    public void TEST_AuthenticatePlayer2()
    {
        AuthenticatePlayer(userName, passWord, PlayerAuthenticated, AuthError);
    }

    public void TEST_RegisterPlayer()
    {
        RegisterPlayer(userName, passWord, PlayerAuthenticated, AuthError);
    }

    public void DeviceAuth(string displayName, OnPlayerAuthenticated onPlayerAuthenticated ,OnError onAuthError)
    {

        new DeviceAuthenticationRequest()
        //.SetDurable(true)
        .SetDisplayName(displayName)
        .Send((response) => {
            if (response.HasErrors)
            {
                Debug.Log("GSM | Device Authentication failed ..." + response.Errors.JSON);

                if (onPlayerAuthenticated != null)
                {
                    onPlayerAuthenticated(new PlayerDetails(response.DisplayName, response.UserId, response.ScriptData));
                }               
            }
            else
            {
                Debug.Log("GSM | Device Authentication suceeded ..." + response.JSONString.ToString());
            }
        });
    }

    public void LogoutTest()
    {
        GS.Reset();
        //Invoke("TEST_AuthenticatePlayer2", 2);
    }

    /// <summary>
    /// SendTestMessage
    /// TEST Method -Delete
    /// </summary>
    public void SendTestMessage()
    {
        GSRequestData msgData = new GSRequestData();
        msgData.Add("SomeKey", "someVar");

        Send_PlayerMessage("59f21651112c29022a9024af", msgData);
    }

    public void DoTestStuff()
    {
        Debug.Log("DoTestStuff");
        /*
        Debug.Log("GSM | RequestTimeout: " + GS.Instance.RequestTimeout);
        Debug.Log("GSM | RetryBase: " + GS.Instance.RetryBase);
        Debug.Log("GSM | RetryMax: " + GS.Instance.RetryMax);
        */
        //StartCoroutine( UploadDownloadableViaREST());

        //GetChallenge("5b913eba27f0d904f4d3f02d", OnGetChallenegeError);

        //Invoke("LogoutTest", 3);

        //List<string> playerList = new List<string>();
        //playerList.Add("59f21651112c29022a9024af");
        //CreateChallenege("TestChal", playerList);

        //Application.OpenURL("http://unity3d.com/");

        //JoinTeam();
        //GetTeam();
        GetTeams("Squad");

        StartCoroutine(CustomCallback("https://t313402b2wv0.preview.gamesparks.net/callback/t313402b2WV0/Custom/ADp0XEymkpwZBcTV2twvpK79ghY1n18v"));

        //List<string> shortCodes = new List<string>();
        //shortCodes.Add("MaxScoreLeaderBoard");
        //GetLeaderBoardEntries(shortCodes, playerDetails.userId);

        //GetOnlinePlayers();
        //EndSession(PlayerEndSessionSuccess, OnEndSessionError);
        //Invoke("GetOnlinePlayers", 1);
        //GetDownloadableURL("Late",OnGetDownloadURL, OnGetDownloadError);
        //GsRequestDataTest();
        //GetLeaderBoard("MaxScoreLeaderBoard", 10);
        //SimpleMatchMaking(OnMatchMakingError);

        //ListMessageDetails();
        //ListMessages();

        //ListChallenges();

        //AccountDetails();
        //EndSession(EndSessionSuccess, OnEndSessionError); ListMessages();

        //HandleDurableRequests();

        //UnityEngine.Object obj = new UnityEngine.Object();
        //string jsonString = GSJson.To(obj);

        //NB!!! CONVERTING SERVER TIMESTAMPS!!!!!
        //CultureInfo currentCulture = Thread.CurrentThread.CurrentCulture;
        //Debug.Log("GSM| DateTime CurrentCulture: " + currentCulture.ToString());
        //Debug.Log("GSM| ConvertUnixTimeStamp(UTC Local): " + "1523008765541 to " + ConvertUnixTimeStamp("1523008765541"));

        Debug.Log("Test stuff finished");
    }

    public void TestSubmitScore()
    {
        SubmitScoreEvent(11);
    }

    public void GetMaxScoreLeaderboard()
    {
        GetLeaderBoard("MaxScoreLeaderBoard", 10);
    }

    void TestClientTimeZone()//Needs Callbacks before being moved out of the TEST region..........
    {
        Debug.Log(DateTime.Now);
        //Debug.Log(DateTime.UtcNow);

        String dateTime = DateTime.Now.ToString();

        new LogEventRequest()
            .SetEventKey("TestModuleSelfRequire")
            .SetEventAttribute("ClientTime", dateTime)
            .Send((queryResponse) => {

                if (!queryResponse.HasErrors)
                {
                    Debug.Log(queryResponse.ScriptData);
                }
                else
                {
                    Debug.Log(queryResponse.Errors.JSON);
                }
            });
    }

    /// <summary>
    /// Im sure this will be usefull later...................
    /// </summary>
    void ListMessages()
    {
        new ListMessageRequest()
        .SetEntryCount(100)
        .Send((response) => {
            if (!response.HasErrors)
            {
                Debug.Log("List of Messages: " + response.JSONString);

                foreach (GSData msg in response.MessageList)
                {
                    
                    //Debug.LogWarning("MsgClass: " + msg.GetString("@class"));
                    //Debug.LogWarning("Sender: " + msg.GetGSData("data").GetString("sender"));
                    //Debug.LogWarning("MsgText: " + msg.GetGSData("data").GetString("textBody"));
                    Debug.Log(msg.GetString("@class"));

                    /*
                    if (msg.GetString("@class") == ".ChallengeTurnTakenMessage")
                    {    
                        //How to fake messages on the client........
                        GSRequestData msgData = new GSRequestData(msg);
                        ChallengeTurnTakenMessage turnMsg = new ChallengeTurnTakenMessage(msgData);
                        //TurnTakenMessageHandler(turnMsg);
                    }
                    */
                }
            }
            else
            {
                Debug.Log("ListMessage ERROR: " + response.Errors.JSON);
            }
        });
    }

    void ListMessageDetails()
    {
        new ListMessageDetailRequest()
       .SetEntryCount(100)
       .SetOffset(0)
       .Send((response) => {
           GSEnumerable<ListMessageDetailResponse._PlayerMessage> messageList = response.MessageList;
           GSData scriptData = response.ScriptData;

           foreach (ListMessageDetailResponse._PlayerMessage msg in response.MessageList)
           {
               if (msg.Seen == false)//Check if the msg has been seen already
               {
                   GSData msgData = new GSData(msg.Message);

                   Debug.Log(msgData.GetString("@class"));

                   if (msgData.GetString("@class") == ".ChallengeTurnTakenMessage")
                   {
                       //GSRequestData reqData = new GSRequestData(msgData);
                       //ChallengeTurnTakenMessage turnMsg = new ChallengeTurnTakenMessage(msgData);
                       //TurnTakenMessageHandler(turnMsg);

                   }
               }
               else
               {
                   Debug.Log("GSM| ListMessageDetailResponse: msgId -" + msg.Id + " SEEN");
               }
           }
       });
    }

    /// <summary>
    /// GetLeaderBoard
    /// </summary>Basic lb getter - will expand later...........................
    /// <param name="leaderboardShortCode"></param>The lbs shortcode
    /// <param name="entryCount"></param> number of entries to be returned
    public void GetLeaderBoard(string leaderboardShortCode, int entryCount)
    {
        //MaxScoreLeaderBoard
        new LeaderboardDataRequest()
        .SetLeaderboardShortCode(leaderboardShortCode)
        .SetEntryCount(entryCount) //num of entries
        //.SetChallengeInstanceId(challengeInstanceId) //If challenge leaderboard
        .SetDontErrorOnNotSocial(false) //if true, error when no friends
        //.SetFriendIds(friendIds) //A friend id or an array of friend ids to use instead of the player's social friends
        .SetIncludeFirst(0) //num from top of lb
        .SetIncludeLast(0) //num from bottom of Lb
        //.SetInverseSocial(inverseSocial) //returns list minus players friends
        .SetOffset(0) //The offset into the set of leaderboards returned
        .SetSocial(false) //If True returns a leaderboard of the player's social friends
        //.SetTeamIds(teamIds) //The IDs of the teams you are interested in
        //.SetTeamTypes(teamTypes) //The type of team you are interested in
        .Send((response) => {

            if (!response.HasErrors)
            {
                Debug.LogWarning(response.JSONString);

                GSEnumerable<LeaderboardDataResponse._LeaderboardData> lbData = response.Data;

                //Debug.Log("LAST: " + response.Last.ToString());
                
                foreach (var data in lbData)
                {
                    Debug.Log(data.JSONString);
                }
            }
            else
            {
                Debug.LogError(response.Errors.JSON);
            }
        });

        /* Vinnies GsEnum method
        var challenged = msg.Challenge.Challenged; //GSEnumerable<ChallengeWonMessage._Challenge._PlayerDetail>
        var enumerator = challenged.GetEnumerator();
        while (enumerator.MoveNext())
        {
            Debug.Log("Challenged Player: " + enumerator.Current.Id);
        }
        */
    }

    public void GetLeaderBoardEntries(List<string> leaderboards, string playerID)
    {
        new LeaderboardsEntriesRequest()
        .SetInverseSocial(false)
        //.SetChallenges(challenges)
        .SetLeaderboards(leaderboards)
        //.SetPlayer(playerID)
        .SetSocial(true)
        //.SetTeamTypes(teamTypes)
        .Send((response) => {

            if (!response.HasErrors)
            {
                Debug.LogWarning(response.JSONString);

                //Get Response data
                GSData responseData = response.BaseData;

                //loop over the leaderboards
                for (int i = 0; i < leaderboards.Count; i++)
                {
                    //Get each leaderboards data
                    List<GSData> lbData = responseData.GetGSDataList(leaderboards[i]);

                    //Check we have entries for this leaderboard
                    if (lbData != null)
                    {
                        //Loop over each leaderboards entries.
                        for (int j = 0; j < lbData.Count; j++)
                        {
                            Debug.LogWarning(lbData[j].JSON);
                            Debug.Log(lbData[j].GetString("userId"));
                            Debug.Log(lbData[j].GetNumber("rank"));
                            Debug.Log(lbData[j].GetNumber("Score"));
                        }
                    }
                    
                }
            }
            else
            {
                Debug.LogError(response.Errors.JSON);
            }
        });
    }

    public void HandleDurableRequests()
    {
        Debug.LogWarning("GSM | DurableQueueRunning: " + GS.Instance.DurableQueueRunning);

        if (GS.DurableQueueCount != 0)
        {
            Debug.LogWarning("GSM | DurableQueueCount Original: " + GS.DurableQueueCount);

            GS.Instance.DurableQueueRunning = false;

            List<GSRequest> requests = GS.Instance.DurableQueueEntries;
            List<GSRequest> newRequests = new List<GSRequest>();

            for (int i = 0; i < requests.Count; i++)
            {
                Debug.LogWarning("GSM | DurableRequest: " + requests[i].JSON);

                newRequests.Add(requests[i]);
                newRequests.Add(requests[i]);

                GS.Instance.RemoveDurableQueueEntry(requests[i]);
            }
            Debug.LogWarning("GSM | newRequests: " + newRequests.Count);


            //reorder newRequests<> HERE if needed

            for (int i = 0; i < newRequests.Count; i++)
            {
                GS.Instance.DurableQueueEntries.Add(newRequests[i]);
            }
            Debug.Log("GSM | newRequests: " + GS.DurableQueueCount);

            GS.Instance.DurableQueueRunning = true;
            Debug.LogWarning("GSM | HandleDurableRequests-GameSparksAvailable " + GS.Available + " DurableQueueRunning:" + GS.Instance.DurableQueueRunning);

        }
    }

    /// <summary>
    /// Test_MatchMakingMessageStorage
    /// </summary>
    /// <param name="data"></param> MatchFoundMessage - Passed as GsData
    void Test_SendJsonData(GSData data)
    {
        GSRequestData requestData = new GSRequestData(data);

        new LogEventRequest()
           .SetEventKey("SendJsonData")
           .SetEventAttribute("Data", requestData)
           .SetDurable(true)
           .Send((testResponse) =>
           {
               if (!testResponse.HasErrors)
               {
                   Debug.Log("GSM | Test_SendJsonData Success: " + testResponse.JSONString);
               }
               else
               {
                   Debug.LogError("GSM | Test_SendJsonData Error: " + testResponse.Errors.JSON);
               }
           });
    }

    public void TicketTest()
    {
        GS.Disconnect();
        Debug.LogWarning("GSM | TicketTest");
        GS.Reconnect();
    }

    #endregion TEST Area<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
}

/*

public static class GameSparksEditorFormUpload
{
    private static readonly Encoding encoding = Encoding.UTF8;

    public static string UploadFile(string url, string fileName, string username, string password)
    {

        FileParameter param = new FileParameter(GetBytesFromFile(fileName), Path.GetFileName(fileName));
        param.FileName = fileName;

        IDictionary<string, object> postParams = new Dictionary<string, object>();
        postParams.Add("binaryContentFile", param);

        return MultipartFormDataPost(url, postParams, username, password);
    }

    public static byte[] GetBytesFromFile(string fullFilePath)
    {
        FileStream fs = null;
        try
        {
            fs = File.OpenRead(fullFilePath);
            byte[] bytes = new byte[fs.Length];
            fs.Read(bytes, 0, Convert.ToInt32(fs.Length));
            return bytes;
        }
        finally
        {
            if (fs != null)
            {
                fs.Close();
                fs.Dispose();
            }
        }

    }

    public static string MultipartFormDataPost(string postUrl, IDictionary<string, object> postParameters, string userName, String password)
    {
        string formDataBoundary = String.Format("----------{0:N}", Guid.NewGuid());
        string contentType = "multipart/form-data; boundary=" + formDataBoundary;

        byte[] formData = GetMultipartFormData(postParameters, formDataBoundary);

        return PostForm(postUrl, contentType, formData, userName, password);

    }

    private static String PostForm(string postUrl, string contentType, byte[] formData, string username, string password)
    {
        HttpWebRequest request = WebRequest.Create(postUrl) as HttpWebRequest;

        if (request == null)
        {
            throw new NullReferenceException("request is not a http request");
        }

        // Set up the request properties.
        request.Method = "POST";
        request.ContentType = contentType;
        request.UserAgent = "Unity Editor";
        request.CookieContainer = new CookieContainer();
        request.ContentLength = formData.Length;

        // You could add authentication here as well if needed:
        request.Headers.Add("Authorization", "Basic " + Convert.ToBase64String(System.Text.Encoding.Default.GetBytes(username + ":" + password)));

        Stream requestStream = request.GetRequestStream();
        requestStream.Write(formData, 0, formData.Length);
        requestStream.Close();

        WebResponse webResponse = request.GetResponse();
        StreamReader responseReader = new StreamReader(webResponse.GetResponseStream());
        return responseReader.ReadToEnd();
    }


    private static byte[] GetMultipartFormData(IDictionary<string, object> postParameters, string boundary)
    {
        Stream formDataStream = new System.IO.MemoryStream();
        bool needsCLRF = false;

        foreach (var param in postParameters)
        {
            // Thanks to feedback from commenters, add a CRLF to allow multiple parameters to be added.
            // Skip it on the first parameter, add it to subsequent parameters.
            if (needsCLRF)
                formDataStream.Write(encoding.GetBytes("\r\n"), 0, encoding.GetByteCount("\r\n"));

            needsCLRF = true;

            if (param.Value is FileParameter)
            {
                FileParameter fileToUpload = (FileParameter)param.Value;

                // Add just the first part of this param, since we will write the file data directly to the Stream
                string header = string.Format("--{0}\r\nContent-Disposition: form-data; name=\"{1}\"; filename=\"{2}\";\r\nContent-Type: {3}\r\n\r\n",
                                              boundary,
                                              param.Key,
                                              fileToUpload.FileName ?? param.Key,
                                              fileToUpload.ContentType ?? "application/octet-stream");

                formDataStream.Write(encoding.GetBytes(header), 0, encoding.GetByteCount(header));

                // Write the file data directly to the Stream, rather than serializing it to a string.
                formDataStream.Write(fileToUpload.File, 0, fileToUpload.File.Length);
            }
            else
            {
                string postData = string.Format("--{0}\r\nContent-Disposition: form-data; name=\"{1}\"\r\n\r\n{2}",
                                                boundary,
                                                param.Key,
                                                param.Value);
                formDataStream.Write(encoding.GetBytes(postData), 0, encoding.GetByteCount(postData));
            }
        }

        // Add the end of the request.  Start with a newline
        string footer = "\r\n--" + boundary + "--\r\n";
        formDataStream.Write(encoding.GetBytes(footer), 0, encoding.GetByteCount(footer));

        // Dump the Stream into a byte[]
        formDataStream.Position = 0;
        byte[] formData = new byte[formDataStream.Length];
        formDataStream.Read(formData, 0, formData.Length);

#if !UNITY_METRO || UNITY_EDITOR
        formDataStream.Close();
#endif

        return formData;
    }

    public class FileParameter
    {
        public byte[] File { get; set; }
        public string FileName { get; set; }
        public string ContentType { get; set; }
        public FileParameter(byte[] file) : this(file, null) { }
        public FileParameter(byte[] file, string filename) : this(file, filename, null) { }
        public FileParameter(byte[] file, string filename, string contenttype)
        {
            File = file;
            FileName = filename;
            ContentType = contenttype;
        }
    }
}
*/
