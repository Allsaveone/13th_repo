using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using Cinemachine;
using System;

public class TimeLinePlayable_ScreenShake : PlayableBehaviour //This is problematic.......
{
    public ExposedReference<CinemachineBrain> brain;

    private CinemachineVirtualCamera virtualCamera;
    public NoiseSettings cameraSway;
    public NoiseSettings cameraShake;
    public float amplitudeGain = 1f;
    public float frequencyGain = 15f;

    private float defualtAmplitudeGain;
    private float defualtFrequencyGain;
    private CinemachineBrain _brain;

    public override void OnGraphStart(Playable playable)
    {
        _brain = brain.Resolve(playable.GetGraph().GetResolver());   
        virtualCamera = (CinemachineVirtualCamera)_brain.ActiveVirtualCamera;
    }

    public override void OnBehaviourPlay(Playable playable, FrameData info)
    {
        defualtAmplitudeGain = virtualCamera.GetComponent<CinemachineBasicMultiChannelPerlin>().m_AmplitudeGain;
        defualtFrequencyGain = virtualCamera.GetComponent<CinemachineBasicMultiChannelPerlin>().m_FrequencyGain;

        virtualCamera.GetComponent<CinemachineBasicMultiChannelPerlin>().m_NoiseProfile  = cameraShake;
        virtualCamera.GetComponent<CinemachineBasicMultiChannelPerlin>().m_AmplitudeGain = amplitudeGain;
        virtualCamera.GetComponent<CinemachineBasicMultiChannelPerlin>().m_FrequencyGain = frequencyGain;
    }

    public override void OnGraphStop(Playable playable)
    {
        virtualCamera.GetComponent<CinemachineBasicMultiChannelPerlin>().m_NoiseProfile = cameraSway;
        virtualCamera.GetComponent<CinemachineBasicMultiChannelPerlin>().m_AmplitudeGain = defualtAmplitudeGain;
        virtualCamera.GetComponent<CinemachineBasicMultiChannelPerlin>().m_FrequencyGain = defualtFrequencyGain;
    }

}