﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ListenerTest : MonoBehaviour {

	void Start ()
    {
        AssignListener();
    }

    void AssignListener()
    {
        SparkManager.instance.OnFocusListener += Listener;
    }

    void Listener (bool hasFocus)
    {
        if (hasFocus)
        {
            Debug.LogWarning("ListenerTest-Focus" + hasFocus);
        }
        else
        {
            Debug.LogWarning("ListenerTest-Focus" + hasFocus);
        }
    }
}
