﻿using GameSparks.Api.Messages;
using GameSparks.Api.Requests;
using GameSparks.Core;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using UnityEngine;
using UnityEngine.Networking;

public class UploadToGameSparks : MonoBehaviour {

    public Sprite upload;


    private void Start()
    {
        //add to GamesparksUnity.cs Start() to Fix GsHelper.Upoload wrapper
        //System.Net.ServicePointManager.ServerCertificateValidationCallback += (o, certificate, chain, errors) => true;
        Debug.Log("UploadToGameSparks");
        UploadCompleteMessage.Listener += UploadCompleteMessageHandler;
        //GetUploadUrl();


        //REST
        //string apiKey = "t313402b2WV0";
        //string url = "https://config2.gamesparks.net/restv2/game/" + "t313402b2WV0" + "/config/~downloadables";
        //byte[] data = System.Text.Encoding.UTF8.GetBytes("TestTestTestTestTestTestTestTest2TheTestening");

        //StartCoroutine(UploadFile(url));//, data));
        //TESTREST();
    }

    private void Update()
    {
        
    }

    public void UploadCompleteMessageHandler(UploadCompleteMessage _message)
    {
        Debug.Log("GSM| Upload Complete! \n " + _message.JSONString);
    }

    void GetUploadUrl ()
    {
        //byte[] data = System.Text.Encoding.UTF8.GetBytes("TestTestTestTestTestTestTestTest2TheTestening");

        new GetUploadUrlRequest()
       //.SetUploadData(data)
       .Send((response) => {

           if (!response.HasErrors)
           {
               Debug.Log(response.JSONString);
               GSData scriptData = response.ScriptData;
               string url = response.Url;

               StartCoroutine(UploadFile(url));//, data));
           }
           else
           {
               Debug.Log(response.Errors.JSON);
           }
       });
    }

    IEnumerator UploadFile(string url)//, byte[] bytes)
    {
        Debug.Log("URL:" + url);

        var form = new WWWForm();

        //form.AddField("somefield", "somedata");
        //form.AddBinaryData("file", bytes, "testText", "text");
        

        UnityWebRequest www = new UnityWebRequest();

        //For Rest
        string username = "david.oneill@GameSparks.com";
        string password = "Articoke2";
        www.SetRequestHeader("Authorization", "Basic " + System.Convert.ToBase64String(System.Text.Encoding.Default.GetBytes(username + ":" + password)));

        //Debug.Log("URL:" + url + www.GetRequestHeader("Authorization").ToString());

        www = UnityWebRequest.Post(url, form);

        yield return www.SendWebRequest();

        if (www.error != null)
        {
            Debug.Log(www.responseCode);// + "\n" + www.GetResponseHeaders().ToString());

            foreach (var item in www.GetResponseHeaders())
            {
                Debug.Log(item.ToString());
            }
        }
        else
        {
            Debug.Log(www.downloadHandler.text);
        }
    }

    void TESTREST()
    {
        byte[] data = System.Text.Encoding.UTF8.GetBytes("TestTestTestTestTestTestTestTest2TheTestening");
        string postUrl = "https://config2.gamesparks.net/restv2/game/" + "t313402b2WV0" + "/config/~downloadables";

        Debug.LogError( PostForm(postUrl, "application/json", data, "david.oneill@gamesparks.com", "Articoke2"));
    }

    private static String PostForm(string postUrl, string contentType, byte[] formData, string username, string password)
    {
        HttpWebRequest request = WebRequest.Create(postUrl) as HttpWebRequest;

        if (request == null)
        {
            throw new NullReferenceException("request is not a http request");
        }

        // Set up the request properties.
        request.Method = "POST";
        request.ContentType = contentType;
        request.UserAgent = "Unity Editor";
        request.CookieContainer = new CookieContainer();
        request.ContentLength = formData.Length;

        // You could add authentication here as well if needed:
        request.Headers.Add("Authorization", "Basic " + Convert.ToBase64String(System.Text.Encoding.Default.GetBytes(username + ":" + password)));
        ///*
        Stream requestStream = request.GetRequestStream();
        requestStream.Write(formData, 0, formData.Length);
        requestStream.Close();
        //*/
        WebResponse webResponse = request.GetResponse();
        StreamReader responseReader = new StreamReader(webResponse.GetResponseStream());
     
        return responseReader.ReadToEnd();
    }
}
