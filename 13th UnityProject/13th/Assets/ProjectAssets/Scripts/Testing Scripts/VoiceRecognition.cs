﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEngine.Windows.Speech;
using System.Linq;
//using UnityEngine.UI;
/*
public class VoiceRecognition : MonoBehaviour {

    //public Text textOutput;

    KeywordRecognizer keyWordRecognizer;
    Dictionary<string, System.Action> keywords = new Dictionary<string, System.Action>();
    PhraseRecognizedEventArgs lastArgs;

    void Start () {

        keywords.Add("Test", () => 
        {
            Test_KeywordCallback();
        });

        keywords.Add("Clear", () =>
        {
            CLear_KeywordCallback();
        });

        keywords.Add("Log In", () =>
        {
            Authenticate_KeywordCallback();
        });

        keyWordRecognizer = new KeywordRecognizer(keywords.Keys.ToArray());
        keyWordRecognizer.OnPhraseRecognized += KeywordRecognizerOnPhraseRecognized;
        keyWordRecognizer.Start();
    }

    void KeywordRecognizerOnPhraseRecognized(PhraseRecognizedEventArgs args)
    {
        System.Action keyWordAction;

        if (keywords.TryGetValue(args.text, out keyWordAction))
        {
            //Debug.Log("OnPhraseRecognized: " + args.text);
            lastArgs = args;
            keyWordAction.Invoke();
        }
    }

    void Test_KeywordCallback ()
    {
        Debug.Log(lastArgs.text);
        //textOutput.text = lastArgs.text;
    }

    void CLear_KeywordCallback()
    {
        Debug.Log(lastArgs.text);
        //textOutput.text = lastArgs.text;
    }

    void Authenticate_KeywordCallback()
    {
        Debug.Log(lastArgs.text);
        //textOutput.text = lastArgs.text;

        if (SparkManager.instance != null)
        {
            SparkManager.instance.TEST_AuthenticatePlayer();
        }
    }
}
*/
