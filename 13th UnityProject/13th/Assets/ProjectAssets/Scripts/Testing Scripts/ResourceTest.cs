﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceTest : MonoBehaviour {

    public string assetName;

	void Start ()
    {
        /*
        if (assetName != null)
        {
            Instantiate(Resources.Load("TestAssets/" + assetName), Vector3.zero, Quaternion.identity);
        }
        */
    }

    bool loadFromResources()
    {
        bool loaded = false;

        if (assetName != null)
        {
            Instantiate(Resources.Load("TestAssets/" + assetName), Vector3.zero, Quaternion.identity);
            loaded = true;
        }

        return loaded;
    }
}
