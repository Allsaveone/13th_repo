﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class WebRequests_TEST : MonoBehaviour {

    IEnumerator Start ()
    {
        yield return new WaitForSeconds(3f);
        StartCoroutine(WebRequest_Get("unityssltest.gamesparks.net"));
        StartCoroutine(WebRequest_Upload("unityssltest.gamesparks.net"));
    }

    public IEnumerator WebRequest_Get(string url)
    {
        Debug.LogWarning("WebRequest_Get");

        UnityWebRequest www = UnityWebRequest.Get(url);
        yield return www.SendWebRequest();

        if (www.isHttpError)
        {
            Debug.LogError("WebRequest_Get Error:" + www.error);
        }
        else
        {
            Debug.LogWarning("WebRequest_Get:" + www.downloadHandler.GetType());
            //byte[] results = www.downloadHandler.data;

            //Switch by Type later...
            /*
            switch (www.downloadHandler.GetType().ToString())
            {
                case "":
                    break;
                default:
                    break;
            }
            */
        }

        Debug.LogWarning("WebRequest_Get - Finished");
    }

    void GetTexture(UnityWebRequest request)
    {
        Texture myTexture = ((DownloadHandlerTexture)request.downloadHandler).texture;
        //Texture myTexture = DownloadHandlerTexture.GetContent(www);
    }

    void GetBundle(UnityWebRequest request)
    {
        AssetBundle bundle = ((DownloadHandlerAssetBundle)request.downloadHandler).assetBundle;
        //AssetBundle bundle = DownloadHandlerAssetBundle.GetContent(www);
    }

    public IEnumerator WebRequest_Upload(string url)
    {
        Debug.LogWarning("WebRequest_Upload");

        List<IMultipartFormSection> formData = new List<IMultipartFormSection>();
        formData.Add(new MultipartFormDataSection("field1=foo&field2=bar"));
        formData.Add(new MultipartFormFileSection("my file data", "myfile.txt"));

        UnityWebRequest www = UnityWebRequest.Post(url, formData);

        //Debug.LogError(www);

        yield return www.SendWebRequest();

        if (www.isHttpError)
        {
            Debug.Log("WebRequest_Upload Error:" + www.error);
        }
        else
        {
            Debug.Log("WebRequest_Upload Complete! - Uploaded to: " + url);
        }

        Debug.Log("WebRequest_Upload - Finished");
    }

    public IEnumerator WebRequest_Put(string url, string putString, bool toBytes = false)
    {
        UnityWebRequest www = new UnityWebRequest();

        if (toBytes)
        {
            www = UnityWebRequest.Put(url,putString);
        }
        else
        {
            byte[] myData = System.Text.Encoding.UTF8.GetBytes(putString);
            www = UnityWebRequest.Put(url, myData);
        }

        yield return www.SendWebRequest();

        if (www.isHttpError)
        {
            Debug.Log("WebRequest_Put Error:" + www.error);
        }
        else
        {
            Debug.Log("Upload complete!");
        }
    }
}
