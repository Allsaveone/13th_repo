﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

public class DataManager : MonoBehaviour {

    public const string SAVEFILEPATH = "/ProjectAssets/Data/SavedGames/";
    public const string SAVEFILEEXTENSION = ".Json";//".13th";

    public List<FileInfo> fileInfo;

    [SerializeField]
    public GameSave loadedGame;

    void Start ()
    {
        //Debug.LogError(Application.persistentDataPath);
        //StartCoroutine( CreateTestSaves(1));//TESTING
        //DeleteFile(Application.dataPath + SAVEFILEPATH, "Test");
        GetSaveGameFiles();
    }

    #region TESTING >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    IEnumerator CreateTestSaves(int delay)
    {

        for (int i = 0; i < 3; i++)
        {
            int index = (i);

            GameSave save = new GameSave("saveFileName_Name_" + index);
            SaveGame(save);
            yield return new WaitForSeconds(delay);
        }
    }

    #endregion >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    #region Saving & Deleteing >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    public void SaveGame(GameSave saveData)
    {
        if (File.Exists(Application.dataPath + SAVEFILEPATH + saveData.saveFileName))
        {
            Debug.LogWarning("Overwriting File: " + saveData.saveFileName); //Prompt "Are you sure?"
        }
        //Debug.LogWarning(Convert.ToDateTime(saveData.date));
        string json = JsonUtility.ToJson(saveData);
        File.WriteAllText(Application.dataPath + SAVEFILEPATH + saveData.saveFileName, json);
    }

    public void GetSaveGameFiles()
    {
        DirectoryInfo dir = new DirectoryInfo(Application.dataPath + SAVEFILEPATH);
        //fileInfo = dir.GetFiles("*"+SAVEFILEEXTENSION).ToList();//Maybe go back to extension check here....("*.*")
        //fileInfo.OrderBy(x => x.LastWriteTimeUtc).Reverse();

        fileInfo = dir.GetFiles("*" + SAVEFILEEXTENSION).OrderByDescending(p => p.LastWriteTime).ToList();//Maybe go back to extension check here....("*.*")

        for (int i = 0; i < fileInfo.Count; i++)
        {
            Debug.Log("Save:" + fileInfo[i].FullName + " " + fileInfo[i].LastWriteTime);// Build Ui here representing the fileInfo - Only File.ReadAllText the specific save being loaded.
            #region Testing
            /*
            string test1 = fileInfo[i].FullName.Substring(fileInfo[i].FullName.IndexOf('_') + 1, fileInfo[i].FullName.IndexOf(".") - (fileInfo[i].FullName.IndexOf('_') + 1));
            Debug.Log(test1);

            string test2 = test1.Substring(test1.IndexOf('_') + 1, test1.Length - (test1.IndexOf('_') + 1));
            Debug.Log(test2);
            */

            //savedGames.Add(LoadSavedGame(fileInfo[i]));//Testing
            //DeleteFile(Application.dataPath + SAVEFILEPATH, fileInfo[i].Name);
            #endregion
        }

        loadedGame = LoadSavedGame(SelectSaveFile(0));
        Debug.LogWarning(loadedGame.randomVariable);
    }

    public FileInfo SelectSaveFile(int index)
    {
        if (fileInfo[index] != null)
        {
            return fileInfo[index];
        }

        return null;
    }

    public GameSave LoadSavedGame(FileInfo file)
    {
        string json = File.ReadAllText(file.ToString());
        GameSave loaded = JsonUtility.FromJson<GameSave>(json);
        return loaded;
    }

    public void DeleteFile(string path, string fileName)
    {
        File.Delete(path + "/" + fileName);
        Debug.LogWarning(fileName + "-Deleted");
    }

    #endregion >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
}

[Serializable]
public struct PlayerSaveData
{
    public PlayerSaveData(string someString, int someInt = 0)
    {

    }
}

[Serializable]
public class GameSave
{
    public string saveFileName { get; private set; }
    public string date { get; private set; }

    public string randomVariable;
    //public PlayerSaveData playerSaveData;

    public GameSave()
    {
        this.saveFileName = "TestSave" + DataManager.SAVEFILEEXTENSION;
        this.date = DateTime.Now.ToString();
        //For testing 
        randomVariable = date;
    }

    public GameSave(string saveFileName)
    {
        this.saveFileName = saveFileName + DataManager.SAVEFILEEXTENSION;
        this.date = DateTime.Now.ToString();

        randomVariable = date;
    }

    public void Save() //Any reason at all this should be a class and not a struct????????????????????????????????????
    {

    }
}
