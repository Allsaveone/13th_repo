﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class VideoContentController : MonoBehaviour {

    public VideoPlayer videoPlayer;//for testuing ads streamed from downloadbles.
    AudioSource audioSource;

    public VideoClip testVideo;

    void Start ()
    {
        audioSource = videoPlayer.GetComponent<AudioSource>();
        videoPlayer.loopPointReached += EndReached;
        videoPlayer.prepareCompleted += GetDuration;

        videoPlayer.gameObject.SetActive(false);

        if (testVideo)
        {
            SetVideoContent(testVideo, true);
        }
    }

    public void SetVideoContent (string url, bool autoPlay = false)
    {
        videoPlayer.source = VideoSource.Url;
        videoPlayer.url = url;
        
        videoPlayer.EnableAudioTrack(0, true);
        videoPlayer.SetTargetAudioSource(0, audioSource);

        if (autoPlay)
        {
            PlayVideoContent();
        }
    }

    public void SetVideoContent(VideoClip videoClip, bool autoPlay = false)
    {
        videoPlayer.source = VideoSource.VideoClip;
        videoPlayer.clip = videoClip;
     
        videoPlayer.SetTargetAudioSource(0, audioSource);
        videoPlayer.EnableAudioTrack(0, false);

        videoPlayer.loopPointReached += EndReached;
        videoPlayer.prepareCompleted += GetDuration;

        if (autoPlay)
        {
            PlayVideoContent();
        }
    }

    public void GetDuration(VideoPlayer vp)
    {
        float duration = videoPlayer.frameCount / videoPlayer.frameRate;
        Debug.Log("Video duration in seconds:" + duration);
    }

    public void EndReached(VideoPlayer vp)
    {
        Debug.LogWarning("Fin!");
        videoPlayer.gameObject.SetActive(false);
    }

    public void PlayVideoContent()
    {
        videoPlayer.gameObject.SetActive(true);

        videoPlayer.Play();
        audioSource.Play();
    }

    public void PauseVideoContent()
    {
        if (videoPlayer.isPlaying)
        {
            videoPlayer.Pause();
            audioSource.Pause();
        }
        else
        {
            videoPlayer.Play();
            audioSource.UnPause();
        }
    }

}
