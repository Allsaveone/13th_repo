﻿using GameSparks.Core;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneHandler : MonoBehaviour {

    public List<string> scenes;

    void OnEnable()
    {
        GetSceneNames();
        SetSceneCallbacks();
        Debug.LogWarning("SH | sceneCountInBuildSettings: " +  SceneManager.sceneCountInBuildSettings);
    }

    void GetSceneNames()
    {
        for (int i = 0; i < SceneManager.sceneCountInBuildSettings; ++i)
        {
            scenes.Add(System.IO.Path.GetFileNameWithoutExtension(SceneUtility.GetScenePathByBuildIndex(i)));
        }
    }
   
    void SetSceneCallbacks()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
        SceneManager.activeSceneChanged += OnActiveSceneChanged;
        SceneManager.sceneUnloaded += OnSceneUnLoaded;
    }

    public Scene GetActiveScene
    {
        get{ return SceneManager.GetActiveScene(); }
    }

    public Scene GetSceneByName(string name)//Scene is a non nullable type....
    {
        Scene scene = SceneManager.GetSceneByName(name);
        return scene;
    }

    #region Scene Delegates >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    public void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        Debug.Log("SH | OnSceneLoaded: " + scene.name + " Mode: " + mode);
        //Debug.Log("SceneHandler | Scenes currently loaded: " + SceneManager.sceneCount);
    }

    public void OnActiveSceneChanged(Scene scene1, Scene scene2)
    {
        Debug.Log("SH | SceneChange: " + scene1.name + " to " + scene2.name);

        //Testing
        //Debug.Log("GS.Authenticated: " + (GS.Authenticated ? "AUTHENTICATED" : "NOT AUTHENTICATED"));
        //SparkManager.Instance().CreateChallenge_Button();
    }

    public void OnSceneUnLoaded(Scene scene)
    {
        Debug.Log("SH | OnSceneUnLoaded: " + scene.name);
        //Debug.Log("SceneHandler | Scenes currently loaded: " + SceneManager.sceneCount);
    }

    #endregion>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    #region Load Scenes >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    /// <summary>
    /// LoadScene
    /// For scenes in the projects Build Settings
    /// </summary>
    /// <param name="sceneName"></param>
    /// <param name="additive"></param>
    public void LoadScene(string sceneName, bool additive = false)
    {
        Debug.Log("SH | LoadScene: " + sceneName + " Mode: " + (additive ? LoadSceneMode.Additive : LoadSceneMode.Single).ToString()); // Only specifying the sceneName or sceneBuildIndex will load the scene with the Single mode

        SceneManager.LoadScene(sceneName, additive ? LoadSceneMode.Additive : LoadSceneMode.Single);
    }

    /// <summary>
    /// LoadSceneAsnc
    /// Load level additive.
    /// </summary>
    /// <param name="sceneName"></param>
    /// <param name="allowSceneActivation"></param> enable the scene when loaded?
    /// <param name="additive"></param> Add to current scene? If false replaces current scene
    /// <returns></returns>
    public IEnumerator LoadSceneAsnc(string sceneName, bool allowSceneActivation = true, bool additive = false)
    {
        Debug.Log("SH | LoadSceneAsnc: " + sceneName + " Mode: " + (additive ? LoadSceneMode.Additive : LoadSceneMode.Single).ToString()); // Only specifying the sceneName or sceneBuildIndex will load the scene with the Single mode

        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneName, additive ? LoadSceneMode.Additive : LoadSceneMode.Single);
        asyncLoad.allowSceneActivation = allowSceneActivation;
        float loadTime = 0;

        while (!asyncLoad.isDone)
        {
            loadTime += Time.deltaTime;
            Debug.Log("SH | LoadSceneAsnc: Loading" + sceneName + " Time:" + loadTime + " Progress:" + asyncLoad.progress + "%");
            yield return new WaitForEndOfFrame();
        }

        Debug.Log("SH | LoadSceneAsnc: Loaded" + sceneName +" Time:" + loadTime);
    }

    #endregion>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    #region Load Scenes - AssetBundle >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    /// <summary>
    /// GetScenePathsFromBundle
    /// Checks a given asset bundle for scene paths
    /// </summary>
    /// <param name="sceneAssetBundle"></param>
    /// <returns></returns>string[] of paths to scenes in the asset bundle - use to get index of scene to use with LoadSceneFromAssetBundle.
    public string[] GetScenePathsFromBundle(AssetBundle sceneAssetBundle)
    {
        string[] paths;//in case of no paths
        paths = sceneAssetBundle.GetAllScenePaths();

        foreach (var path in paths)
        {
            Debug.Log("SH | LoadSceneFromAssetBundle: Path- " + path + " found in Asset bundle " + sceneAssetBundle);
        }

        return paths;
    }

    /// <summary>
    /// LoadSceneFromAssetBundle#
    /// For scenes loaded from asset bundles.
    /// </summary>
    /// <param name="sceneAssetBundle"></param> the asset bundle containing scenes
    /// <param name="additive"></param> Add to current scene? If false replaces current scene
    /// <param name="index"></param> index of the scene path to use
    public void LoadSceneFromAssetBundle(AssetBundle sceneAssetBundle, int index = 0, bool additive = false)
    {
        Debug.Log("SH | LoadSceneFromAssetBundle: " + sceneAssetBundle.name);

        #region AssetCheck
        /*
        string[] assets = sceneAssetBundle.GetAllAssetNames();
        foreach (var asset in assets)
        {
            Debug.Log("SceneHandler | LoadSceneFromAssetBundle: Asset- " + asset + " found in Asset bundle " + sceneAssetBundle);
        }
        */
        #endregion

        string[] scenePaths = GetScenePathsFromBundle(sceneAssetBundle);

        if (scenePaths[0] == null)
        {
            Debug.LogError("SH | LoadSceneFromAssetBundle: ERROR: No scenes found in Asset bundle: " + sceneAssetBundle);
            return;
        }

        SceneManager.LoadScene(scenePaths[index], additive ? LoadSceneMode.Additive : LoadSceneMode.Single);//load first scene from bundle
    }

    #endregion>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    void OnDisable()
    {
        StopAllCoroutines();    
    }
}
