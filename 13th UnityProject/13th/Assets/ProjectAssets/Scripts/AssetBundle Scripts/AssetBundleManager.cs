﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class AssetBundleManager : MonoBehaviour {

    #region Singleton  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    public static AssetBundleManager Instance { get; private set; }

    void Awake()
    {
        if (Instance == null)
        {
            Debug.Log("ABM| Singleton Initialized...");
            Instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            //There can be only one!
            Debug.Log("ABM| Removed Duplicate...");
            Destroy(this.gameObject);
        }
    }

    #endregion >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    //public int contentVersion;//need this later.... probably.....
    [Header("AssetBundle Content")]
    public List<DlcContent> dlcAssets;

    #region Saving & Deleteing >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    public void SaveFile(string path, string fileName, byte[] bytes)
    {
        if (File.Exists(path + "/" + fileName))
        {
            Debug.LogWarning("ABM| Overwriting File: " + fileName); //Prompt "Are you sure?"... etc
        }

        File.WriteAllBytes(path + "/" + fileName, bytes);
        Debug.LogWarning("ABM| SimpleSaveFile " + fileName + " @" + path);
    }

    public void DeleteFile(string path, string fileName)
    {
        Debug.LogWarning("ABM| Attempt DeleteFile: " + fileName);

        if (CheckFileExists(path + fileName))
        {
            File.Delete(path + "/" + fileName);
            Debug.LogWarning("ABM| File: " + fileName + " Exists -Deleted File " +  " @" + path);
        }
        else
        {
            Debug.LogError("ABM| File: " + fileName +" does not Exist"  + " @" + path);
        }

    }

    #endregion >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    #region Asset Bundles >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    /// <summary>
    /// RetrieveAssetBundles
    /// </summary>
    /// <param name="assetsToLoad"></param> a list of the assets to be loaded from cache or downloaded if not present
    /// <returns></returns>
    public IEnumerator RetrieveAssetBundles(List<string> assetsToLoad)
    {
        //DeleteFile((Application.persistentDataPath + "/"), assetsToLoad[0]);

        if (assetsToLoad.Count == 0)
        {
            Debug.LogWarning("ABM| RetrieveAssetBundles - No Assets To Load");
        }
        else
        {
            for (int i = 0; i < assetsToLoad.Count; i++)
            {
                string localFilePath = Application.persistentDataPath + "/" + assetsToLoad[i];
                Debug.Log(localFilePath);

                if (CheckFileExists(localFilePath))
                {
                    var ccc = WWW.LoadFromCacheOrDownload("file://" + localFilePath, 0);
                    yield return ccc;

                    Debug.Log("ABM| loading " + assetsToLoad[i] + " from local cache:" + ccc.url);
                    yield return ExtractAssetsFromBundle(ccc.assetBundle, 0);
                }
                else
                {
                    Debug.LogError("ABM| Retriving url from GameSparks(Manager) for " + assetsToLoad[i]);//This assumes bundles names are used as ShortCodes for their downloadables.
                    //No local cached version found - proceed to get Dlc url and download
                    SparkManager.instance.GetDownloadableURL(assetsToLoad[i], SparkManager.instance.OnGetDownloadURL, SparkManager.instance.OnGetDownloadError);
                }
            }
        }    
    }

    /// <summary>
    /// CheckFileExists
    /// ...does Exactly what it says on the tin...
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns> true is file exists....False if not...
    public bool CheckFileExists(string path)
    {
        if (System.IO.File.Exists(path))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /// <summary>
    /// LoadAssetBundle
    /// Attempts to load a file from cache, if not found in the cache it will download the file using the url
    /// </summary>
    /// <param name="fileName"></param>
    /// <param name="url"></param>
    /// <returns></returns>
    public IEnumerator LoadAssetBundle(string fileName, string url)
    {
        float loadTime = 0;
        loadTime += Time.deltaTime;

        string localFilePath = Application.persistentDataPath + "/" + fileName;
        Debug.Log("ABM| LoadAssetBundle - localFilePath: " + localFilePath);

        if (CheckFileExists(localFilePath))
        {
            var ccc = WWW.LoadFromCacheOrDownload("file://" + localFilePath, 0);
            yield return ccc;

            Debug.Log("ABM| LoadAssetBundle - local");
            yield return ExtractAssetsFromBundle(ccc.assetBundle, "SphereDlc", loadTime); //Load specific asset from bundle
            //yield return ExtractAssetsFromBundle(ccc.assetBundle, loadTime); //load whole bundle
        }
        else
        {
            WWW www = new WWW(url);
            yield return www;

            SaveFile(Application.persistentDataPath, fileName, www.bytes);
            Debug.Log("ABM| LoadAssetBundle - downloaded from GameSparks and saved to device:" + www.url + " Operation time:" + loadTime);
        }
    }

    /// <summary>
    /// ExtractAssetsFromBundle.
    /// Load all assets from bundle.
    /// </summary>
    /// <param name="bundle"></param>
    /// <param name="loadTime"></param>
    /// <returns></returns>
    IEnumerator ExtractAssetsFromBundle(AssetBundle bundle, float loadTime)
    {
        loadTime += Time.deltaTime;

        if (bundle == null)
        {
            Debug.Log("ABM| ExtractAssetsFromBundle - Bundle is Null. Operation time:" + loadTime);
            yield return null;
        }

        GameObject[] loadedPrefabs = bundle.LoadAllAssets<GameObject>();// type check!!! what if other asset types??

        yield return loadedPrefabs;
        Debug.Log("ABM| ExtractAssetsFromBundle- " + bundle.name + " Retrieved Successfully! Operation time:" + loadTime);

        foreach (GameObject prefab in loadedPrefabs)
        {
            DlcContent dlc = new DlcContent();
            dlc.id = prefab.name;
            dlc.prefab = prefab;

            dlcAssets.Add(dlc);
        }

        bundle.Unload(false);
        Debug.Log("ABM| ExtractAssetsFromBundle- loaded Assets from Bundle Successfully! Operation time:" + loadTime);

        AssetBundleManager.Instance.TestSpawn();
    }


    /// <summary>
    /// ExtractAssetsFromBundle
    /// ExtractAssetsFromBundle - extract specific asset
    /// </summary>
    /// <param name="bundle"></param>
    /// <param name="assetToLoad"></param>
    /// <param name="loadTime"></param>
    /// <returns></returns>
    IEnumerator ExtractAssetsFromBundle(AssetBundle bundle, string assetToLoad, float loadTime)
    {
        loadTime += Time.deltaTime;

        if (bundle == null)
        {
            Debug.Log("ABM| ExtractAssetsFromBundle - Bundle is Null. Operation time:" + loadTime);
            yield return null;
        }

        GameObject[] loadedPrefabs = bundle.LoadAllAssets<GameObject>();// type check!!! what if other asset types??
        yield return loadedPrefabs;
        Debug.Log("ABM| ExtractAssetsFromBundle- " + bundle.name + " Retrieved Successfully! Operation time:" + loadTime);

        for (int i = 0; i < loadedPrefabs.Length; i++)
        {
            if (loadedPrefabs[i].name == assetToLoad)
            {
                DlcContent dlc = new DlcContent();
                dlc.id = loadedPrefabs[i].name;
                dlc.prefab = loadedPrefabs[i];

                AssetBundleManager.Instance.dlcAssets.Add(dlc);
            }
        }

        bundle.Unload(false);
        Debug.Log("ABM| ExtractAssetsFromBundle- loaded Assets from Bundle Successfully! Operation time:" + loadTime);

        TestSpawn();
    }

    /// <summary>
    /// AsyncExtractAssetsFromBundle
    /// Async load All Assets From Bundle
    /// </summary>
    /// <param name="bundle"></param>
    /// <param name="loadTime"></param>
    /// <returns></returns>
    IEnumerator AsyncExtractAssetsFromBundle(AssetBundle bundle, float loadTime)
    {
        loadTime += Time.deltaTime;

        if (bundle == null)
        {
            Debug.Log("ABM| AsyncExtractAssetsFromBundle- Bundle is Null. Operation time:" + loadTime);
            yield return null;
        }

        //bundle.Contains("name");
        //bundle.LoadAllAssets<AudioClip>();
        AssetBundleRequest asyncAssetLoad = bundle.LoadAllAssetsAsync<GameObject>();

        while (!asyncAssetLoad.isDone)
        {
            Debug.Log("ABM| AsyncExtractAssetsFromBundle- Loading" + loadTime + " Progress:" + asyncAssetLoad.progress + "%");
            yield return new WaitForEndOfFrame();
        }

        if (asyncAssetLoad.allAssets[0] == null)
        {
            Debug.Log("ABM| AsyncExtractAssetsFromBundle - AssetBundleRequest is Null no assets of Type GameObject. Operation time:" + loadTime);
            yield return null;
        }

        Debug.Log("ABM| AsyncExtractAssetsFromBundle-" + bundle.name + " Retrieved Successfully! Operation time:" + loadTime);

        foreach (GameObject prefab in asyncAssetLoad.allAssets)
        {
            DlcContent dlc = new DlcContent();
            dlc.id = prefab.name;
            dlc.prefab = prefab;

            AssetBundleManager.Instance.dlcAssets.Add(dlc);
        }

        bundle.Unload(false);
        Debug.Log("ABM| AsyncExtractAssetsFromBundle -loaded Assets from Bundle Successfully! Operation time:" + loadTime);

        AssetBundleManager.Instance.TestSpawn();
    }


    /// <summary>
    /// AsyncExtractAssetsFromBundle
    /// Async load All Assets From Bundle
    /// </summary>
    /// <param name="bundle"></param>
    /// <param name="loadTime"></param>
    /// <returns></returns>
    IEnumerator AsyncExtractAssetsFromBundle(AssetBundle bundle, string assetToLoad, float loadTime)
    {
        loadTime += Time.deltaTime;

        if (bundle == null)
        {
            Debug.Log("ABM| AsyncExtractAssetsFromBundle- Bundle is Null. Operation time:" + loadTime);
            yield return null;
        }

        //bundle.Contains("name");
        //bundle.LoadAllAssets<AudioClip>();
        AssetBundleRequest asyncAssetLoad = bundle.LoadAllAssetsAsync<GameObject>();

        while (!asyncAssetLoad.isDone)
        {
            Debug.Log("ABM| AsyncExtractAssetsFromBundle- Loading" + loadTime + " Progress:" + asyncAssetLoad.progress + "%");
            yield return new WaitForEndOfFrame();
        }

        if (asyncAssetLoad.allAssets[0] == null)
        {
            Debug.Log("ABM| AsyncExtractAssetsFromBundle - AssetBundleRequest is Null no assets of Type GameObject. Operation time:" + loadTime);
            yield return null;
        }

        Debug.Log("ABM| AsyncExtractAssetsFromBundle-" + bundle.name + " Retrieved Successfully! Operation time:" + loadTime);
  
        
        for (int i = 0; i < asyncAssetLoad.allAssets.Length; i++)
        {
            if (asyncAssetLoad.allAssets[i].name == assetToLoad)
            {
                DlcContent dlc = new DlcContent();
                dlc.id = asyncAssetLoad.allAssets[i].name;
                dlc.prefab = (GameObject)asyncAssetLoad.allAssets[i];

                AssetBundleManager.Instance.dlcAssets.Add(dlc);
            }
        }
       
        /* From Async load all
        foreach (GameObject prefab in asyncAssetLoad.allAssets)
        {
            DlcContent dlc = new DlcContent();
            dlc.id = prefab.name;
            dlc.prefab = prefab;

            AssetBundleManager.Instance.dlcAssets.Add(dlc);
        }
        */

        bundle.Unload(false);
        Debug.Log("ABM| AsyncExtractAssetsFromBundle -loaded Assets from Bundle Successfully! Operation time:" + loadTime);

        AssetBundleManager.Instance.TestSpawn();
    }

    #endregion >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    #region Testing >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    /*
    IEnumerator Start()//TESTING -
    {
        yield return RetrieveAssetBundles(assetsToLoad);
    }
    */
    public void TestSpawn()
    {
        for (int i = 0; i < dlcAssets.Count; i++)
        {
            Instantiate(dlcAssets[i].prefab, Vector3.zero, Quaternion.identity);
            //Debug.LogWarning("ABM| Spawned : " + dlcAssets[i].prefab);
        }
    }

    #endregion >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
}

[System.Serializable]
public class DlcContent {

    public string id; //for getting model by id later, name for now....
    public GameObject prefab;
}

