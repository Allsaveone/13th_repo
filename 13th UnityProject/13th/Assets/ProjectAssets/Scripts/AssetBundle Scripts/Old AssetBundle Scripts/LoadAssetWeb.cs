﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Networking;

public class LoadAssetWeb : MonoBehaviour {

    [Header("TESTING")]
    public List <string> assetsToLoad;//TESTING

    IEnumerator Start()//TESTING - Rename Later And pass a list of strings directly to the method.
    {
        for (int i = 0; i < assetsToLoad.Count; i++)
        {
            string localFilePath = Application.persistentDataPath + "/" + assetsToLoad[i];

            if (CheckFileExists(localFilePath))
            {
                Debug.LogWarning("Loading LocalAssets");
                var ccc = WWW.LoadFromCacheOrDownload("file://" + localFilePath, 0);
                yield return ccc;

                Debug.Log("LoadAssetBundle| loading " + assetsToLoad + " from local cache:" + ccc.url);
                yield return  AsyncExtractAssetsFromBundle(ccc.assetBundle, 0);
                //yield return ExtractAssetsFromBundle(ccc.assetBundle, 0);
            }
            else
            {
                Debug.LogError("YURT");//............................................................................
                //No local cached version found - proceed to get Dlc url and download
            }
        }     
    }

    public bool CheckFileExists(string path)
    {
        if (System.IO.File.Exists(path))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /// <summary>
    /// LoadAssetBundle
    /// Attempts to load a file from cache, if not found will retrieve using url
    /// </summary>
    /// <param name="fileName"></param>
    /// <param name="url"></param>
    /// <returns></returns>
    public IEnumerator LoadAssetBundle(string fileName, string url)
    {
        float loadTime = 0;
        loadTime += Time.deltaTime;

        string localFilePath = Application.persistentDataPath + "/" + fileName;
        Debug.Log(localFilePath);

        if (CheckFileExists(localFilePath))
        {
            //AssetBundleManager.instance.DeleteFile((Application.persistentDataPath + "/"), fileName);
            
            var ccc = WWW.LoadFromCacheOrDownload("file://" + localFilePath,0);
            yield return ccc;

            Debug.Log("LoadAssetBundle| loading from local cache:" + ccc.url);
            yield return ExtractAssetsFromBundle(ccc.assetBundle,"SphereDlc",loadTime); //Load specific asset from bundle
            //yield return ExtractAssetsFromBundle(ccc.assetBundle, loadTime); //load whole bundle
        }
        else
        {
            var www = new WWW(url);
            yield return www;

            AssetBundleManager.Instance.SaveFile(Application.persistentDataPath, fileName, www.bytes);
            Debug.Log("LoadAssetBundle| downloaded from cloud data and saved to device:" + www.url + " Operation time:" + loadTime);
        }
    }

    /// <summary>
    /// ExtractAssetsFromBundle.
    /// Load all assets from bundle.
    /// </summary>
    /// <param name="bundle"></param>
    /// <param name="loadTime"></param>
    /// <returns></returns>
    IEnumerator ExtractAssetsFromBundle(AssetBundle bundle, float loadTime)
    {
        loadTime += Time.deltaTime;

        if (bundle == null)
        {
            Debug.Log("ExtractAssetsFromBundle| Bundle is Null. Operation time:" + loadTime);
            yield return null;
        }

        GameObject[] loadedPrefabs = bundle.LoadAllAssets<GameObject>();// type check!!! what if other asset types??

        yield return loadedPrefabs;
        Debug.Log("ExtractAssetsFromBundle| " + bundle.name + " Retrieved Successfully! Operation time:" + loadTime);

        foreach (GameObject prefab in loadedPrefabs)
        {
            DlcContent dlc = new DlcContent();
            dlc.id = prefab.name;
            dlc.prefab = prefab;

            AssetBundleManager.Instance.dlcAssets.Add(dlc);
        }

        bundle.Unload(false);
        Debug.Log("ExtractAssetsFromBundle| loaded Assets from Bundle Successfully! Operation time:" + loadTime);

        AssetBundleManager.Instance.TestSpawn();
    }

    IEnumerator AsyncExtractAssetsFromBundle(AssetBundle bundle, float loadTime)
    {
        loadTime += Time.deltaTime;

        if (bundle == null)
        {
            Debug.Log("AsyncExtractAssetsFromBundle| Bundle is Null. Operation time:" + loadTime);
            yield return null;
        }

        //bundle.Contains("name");
        //bundle.LoadAllAssets<AudioClip>();
        AssetBundleRequest asyncAssetLoad = bundle.LoadAllAssetsAsync<GameObject>();

        while (!asyncAssetLoad.isDone)
        {
            Debug.Log("AsyncExtractAssetsFromBundle: Loading" + loadTime + " Progress:" + asyncAssetLoad.progress + "%");
            yield return new WaitForEndOfFrame();
        }

        if (asyncAssetLoad.allAssets[0] == null)
        {
            Debug.Log("AsyncExtractAssetsFromBundle| AssetBundleRequest is Null no assets of Type GameObject. Operation time:" + loadTime);
            yield return null;
        }

        Debug.Log("AsyncExtractAssetsFromBundle| " + bundle.name + " Retrieved Successfully! Operation time:" + loadTime);

        foreach (GameObject prefab in asyncAssetLoad.allAssets)
        {
            DlcContent dlc = new DlcContent();
            dlc.id = prefab.name;
            dlc.prefab = prefab;

            AssetBundleManager.Instance.dlcAssets.Add(dlc);
        }

        bundle.Unload(false);
        Debug.Log("AsyncExtractAssetsFromBundle| loaded Assets from Bundle Successfully! Operation time:" + loadTime);

        AssetBundleManager.Instance.TestSpawn();
    }

    /// <summary>
    /// ExtractAssetsFromBundle
    /// Load a specific asset from a bundle by name.
    /// </summary>
    /// <param name="bundle"></param>
    /// <param name="assetToLoad"></param>
    /// <param name="loadTime"></param>
    /// <returns></returns>
    IEnumerator ExtractAssetsFromBundle(AssetBundle bundle, string assetToLoad, float loadTime)
    {
        loadTime += Time.deltaTime;

        if (bundle == null)
        {
            Debug.Log("ExtractAssetsFromBundle| Bundle is Null. Operation time:" + loadTime);
            yield return null;
        }

        GameObject[] loadedPrefabs = bundle.LoadAllAssets<GameObject>();// type check!!! what if other asset types??
        yield return loadedPrefabs;
        Debug.Log("ExtractAssetsFromBundle| " + bundle.name + " Retrieved Successfully! Operation time:" + loadTime);

        for (int i = 0; i < loadedPrefabs.Length; i++)
        {
            if (loadedPrefabs[i].name == assetToLoad)
            {
                DlcContent dlc = new DlcContent();
                dlc.id = loadedPrefabs[i].name;
                dlc.prefab = loadedPrefabs[i];

                AssetBundleManager.Instance.dlcAssets.Add(dlc);
            }
        }

        bundle.Unload(false);
        Debug.Log("ExtractAssetsFromBundle| loaded Assets from Bundle Successfully! Operation time:" + loadTime);

        AssetBundleManager.Instance.TestSpawn();
    }

    /*
    public IEnumerator LoadFromCasheOrDownload(string url)
    {
        var www = WWW.LoadFromCacheOrDownload(url, 0);
        yield return www;

        AssetBundle downloadedAssetBundle = www.assetBundle;

        if (downloadedAssetBundle == null)
        {
            Debug.Log("LoadFromCasheOrDownload| ???");
            yield return null;
        }
        Debug.LogWarning("LoadFromCasheOrDownload| Downloaded " + www.assetBundle.name + " Successfully! Operation time:");

        GameObject[] loadedPrefabs = downloadedAssetBundle.LoadAllAssets<GameObject>();// type check!!! what if other asset types??
        yield return loadedPrefabs;

        Debug.Log("LoadFromCasheOrDownload| Loaded Prefabs ");

        foreach (GameObject prefab in loadedPrefabs)
        {
            Debug.LogWarning("LoadFromCasheOrDownload| LoadedBundle Contains: " + prefab.name);

            DlcContent dlc = new DlcContent();
            dlc.id = prefab.name;
            dlc.prefab = prefab;

            AssetBundleManager.instance.dlcAssets.Add(dlc);
        }

        downloadedAssetBundle.Unload(false);

        AssetBundleManager.instance.TestSpawn();
        //AssetBundleManager.instance.SimpleSaveFile("Assets","COCK",www.assetBundle.);
        //yield return AssetBundleManager.instance.SaveDownload(www.url);
    }

    public IEnumerator LoadBundleFromWeb(string bundleName, string url)
    {
        Debug.Log("LoadAssetWeb| LoadBundleFromWeb:" + bundleName);
        float loadTime = 0;
        loadTime += Time.deltaTime;

        using (WWW www = new WWW(url))
        {
            //Debug.LogError("URL" + url);

            yield return www;

            if (www.error != null)
            {
                Debug.LogError("LoadAssetWeb| Failed to LoadBundle " + bundleName + " from " + www + "-Download Error:" + www.error.ToString() + " Operation time:" + loadTime);
                yield break;
            }

            AssetBundle downloadedAssetBundle = www.assetBundle;

            if (downloadedAssetBundle == null)
            {
                yield return null;
            }
            Debug.LogWarning("LoadAssetWeb| Downloaded " + www.assetBundle.name + " Successfully! Operation time:" + loadTime);


            GameObject[] loadedPrefabs = downloadedAssetBundle.LoadAllAssets<GameObject>();// type check!!! what if other asset types??
            yield return loadedPrefabs;
            Debug.Log("LoadAssetWeb| Loaded Prefabs " + " Operation time:" + loadTime);

            foreach (GameObject prefab in loadedPrefabs)
            {
                Debug.LogWarning("LoadAssetWeb| LoadedBundle Contains: " + prefab.name);

                DlcContent dlc = new DlcContent();
                dlc.id = prefab.name;
                dlc.prefab = prefab;

                AssetBundleManager.instance.dlcAssets.Add(dlc);
            }

            downloadedAssetBundle.Unload(false);

            AssetBundleManager.instance.TestSpawn();
        }
    }
    */

    void OnDisable()
    {
        StopAllCoroutines();
    }
}
