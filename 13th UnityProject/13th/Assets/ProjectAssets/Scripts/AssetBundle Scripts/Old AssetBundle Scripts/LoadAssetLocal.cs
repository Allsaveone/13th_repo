﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class LoadAssetLocal : MonoBehaviour {    //For Streaming Assets............ Will move this to LoadAssetWeb then into the AssetBundleManager

    public string assetBundleName;

	IEnumerator Start () {
        Debug.Log("LoadAssetLocal| Init");
        yield return LoadBundle(assetBundleName);
	}

    IEnumerator LoadBundle(string bundleName)
    {
        Debug.Log("LoadAssetLocal| LoadBundle:" + bundleName);
        float loadTime = 0;
        loadTime += Time.deltaTime;

        string path = Path.Combine(Application.streamingAssetsPath, bundleName);

        if (!File.Exists(path))
        {
            Debug.LogError("LoadAssetLocal| Failed to LoadBundle:" + bundleName + " NotFound! Operation time:" + loadTime);
            yield break;
        }

        AssetBundleCreateRequest loadingAssetBundle = AssetBundle.LoadFromFileAsync(path);
        yield return loadingAssetBundle;

        AssetBundle loadedAssetBundle = loadingAssetBundle.assetBundle;

        if (loadedAssetBundle == null)
        {
            Debug.LogError("LoadAssetLocal| Failed to LoadBundle:" + loadedAssetBundle.ToString() + " Operation time:" + loadTime);
            yield break;
        }

        GameObject[] loadedPrefabs = loadedAssetBundle.LoadAllAssets<GameObject>();
        yield return loadedPrefabs;
        Debug.Log("LoadAssetLocal| Loaded Prefabs " + " Operation time:" + loadTime);

        foreach (GameObject prefab in loadedPrefabs)
        {
            Debug.LogWarning("LoadAssetLocal| LoadedBundle Contains: " + prefab.name);

            DlcContent dlc = new DlcContent();
            dlc.id = prefab.name;
            dlc.prefab = prefab;

            AssetBundleManager.Instance.dlcAssets.Add(dlc);
        }

        loadedAssetBundle.Unload(false);
        //ResourceManager.instance.TestSpawn();//TESTING!
        //Debug.LogError("LoadAssetLocal| " + bundleName + "Complete! Operation time:" + loadTime);

        if (File.Exists(path + "-Mod"))
        {
            yield return LoadMod(bundleName + "-Mod",loadTime);
        }

        Debug.LogError("LoadAssetLocal| " + bundleName + "Complete! Operation time:" + loadTime);
    }

    IEnumerator LoadMod(string modName,float loadTime)
    {
        Debug.Log("LoadAssetLocal| Loading Mod:" + modName);
        AssetBundleCreateRequest loadingModAssetBundle = AssetBundle.LoadFromFileAsync(modName);
        yield return loadingModAssetBundle;

        AssetBundle loadedAssetBundle = loadingModAssetBundle.assetBundle;

        if (loadedAssetBundle == null)
        {
            Debug.LogError("LoadAssetLocal| Failed to LoadMod:" + loadedAssetBundle.ToString() + " -No Mod found- Operation time:" + loadTime);
            yield break;
        }
        Debug.Log("LoadAssetLocal| AssetBundleCreateRequest Processed Local Load " + " Operation time:" + loadTime);

        GameObject[] loadedPrefabs = loadedAssetBundle.LoadAllAssets<GameObject>();
        yield return loadedPrefabs;
        Debug.Log("LoadAssetLocal| LoadedMod " + " Operation time:" + loadTime);
        
        ///* Mod assets - 
        foreach (GameObject prefab in loadedPrefabs)
        {
            Debug.LogWarning("LoadAssetLocal| LoadedBundle Contains: " + prefab.name);

            DlcContent dlc = new DlcContent();
            dlc.id = prefab.name;
            dlc.prefab = prefab;

            AssetBundleManager.Instance.dlcAssets.Add(dlc);//make gameAssets a Class? Dlc?
        }
        //*/

        loadedAssetBundle.Unload(false);
        //ResourceManager.instance.TestSpawn();//TESTING!
    }

    void OnDisable()
    {
        StopAllCoroutines();
    }
}
