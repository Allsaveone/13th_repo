﻿using Cinemachine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cinemachine_CameraManager : MonoBehaviour {

    /// <summary>
    /// Non-Timeline dependant Cinemachine SceenShake
    /// </summary>
    [Header("CineMachine Brain")]
    public CinemachineBrain cameraBrain;

    [Header("Virtual Cameras")]
    public CinemachineVirtualCamera virtualCamera;

    [Header("ScreenShake Settings")]
    public float amplitudeGain = 1f;
    public float frequencyGain = 1f;
    public float shakeDuration = 1f;
    public NoiseSettings cameraSway;
    public NoiseSettings cameraShake;
    public bool shaking = false;
    public float shakeTimer = 0;

    void Start()
    {
        shaking = false;
        virtualCamera = (CinemachineVirtualCamera)cameraBrain.ActiveVirtualCamera;
        virtualCamera.GetComponent<CinemachineBasicMultiChannelPerlin>().m_NoiseProfile = cameraSway;
        //CinemachineBasicMultiChannelPerlin a = virtualCamera.AddCinemachineComponent<CinemachineBasicMultiChannelPerlin>();//<CinemachineBasicMultiChannelPerlin>();
        //virtualCamera.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_NoiseProfile = mynoisedef;

        Debug.Log("Cinemachine_CameraManager-Start");

        InvokeRepeating("TEST_Shake", 2, 2);
    }

    public void TEST_Shake()
    {
        StopAllCoroutines();
        Debug.Log("TEST_Shake");
        StartCoroutine(Shake(virtualCamera, shakeDuration));
    }

    IEnumerator Shake(CinemachineVirtualCamera vCamera, float shakeTime)
    {
        shakeTimer = 0;
        virtualCamera.GetComponent<CinemachineBasicMultiChannelPerlin>().m_NoiseProfile = cameraShake;
        virtualCamera.GetComponent<CinemachineBasicMultiChannelPerlin>().m_AmplitudeGain = amplitudeGain;
        virtualCamera.GetComponent<CinemachineBasicMultiChannelPerlin>().m_FrequencyGain = frequencyGain;
        shaking = true;

        while (shakeTimer <= shakeTime)
        {
            shakeTimer += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        virtualCamera.GetComponent<CinemachineBasicMultiChannelPerlin>().m_NoiseProfile = cameraSway;
        virtualCamera.GetComponent<CinemachineBasicMultiChannelPerlin>().m_AmplitudeGain = 1;
        virtualCamera.GetComponent<CinemachineBasicMultiChannelPerlin>().m_FrequencyGain = 1;
        shaking = false;
        shakeTimer = 0;
    }

    public void CameraCutEvent()
    {
        //Debug.Log("CCM | CameraCutEvent:" + cameraBrain.ActiveVirtualCamera.Name);
    }

    public void CameraActivatedEvent()
    {
        //Debug.Log("CCM | CameraActivatedEvent:" + cameraBrain.ActiveVirtualCamera.Name);
    }

    void OnTransitionFromCamera(ICinemachineCamera fromCam)
    {
        //Debug.Log("CCM | OnTransitionFromCamera:" + fromCam);
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }
}
